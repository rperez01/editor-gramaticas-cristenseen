/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package Compiler;

import ExternalComunication.SyntaxHolder;
import ExternalComunication.SyntaxRule;
import ExternalComunication.SyntaxSymbol;
import InternalLogic.JavaCode;
import InternalLogic.Project;
import InternalLogic.SyntaxHeader;
import InternalLogic.SyntaxLine;
import JFlexGeneratedFiles.JFlexRenamer;
import static JFlexGeneratedFiles.JFlexRenamer.YYINITIAL;
import ide.gc.globalvalues.SymbolsStorage;
import ide.gc.ui.JPConsole;
import ide.gc.ui.MainWindow;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *Compila la semántica del proyecto
 * @author ricardo
 */
public class SemanticsCompiler {
    private String className;
    public static final String FILE_EXTENSION ="java"; 
    private int maxSimbolsRight;
    public SemanticsCompiler(String className){
        this.className=className;
    }
    
    public void SetClassName(String className){
        this.className=className;
    }
    
    public String CompilateProject(Project project,SyntaxHolder sh) throws CompilerException{
        String ret="";
        ret+="package gemma_esqrew;\n\n"
                +getImports()
                +"\n"
                +"public class "+className+"{\n"
                +printRules(sh)
                +"private static boolean reglasConstruidas = false;\n\n"
                +MainWindow.getWindow().getJPAuxiliarDisplay().getSemanticHeader().getText()
                +"\n"
                +"public "+ className+ "(){}\n\n"
                +"private static void construirReglas(){\n"
                +"NoTerminal parteIzquierda;\n"
                +"CadenaSimbolos parteDerecha;\n\n"
                + "reglasConstruidas= true;\n\n"
                + printConstruirReglas(sh)
                +"}\n\n"
                + "public static boolean accionSemantica(ArbolAnalisis arbol,\n"
                + "                                      Integer atributosSintetizados,\n"
                + "                                      Integer atributosHeredadosPadre,\n"
                + "                                      Integer atributosHeredadosHermanos,\n"
                + "                                      Regla regla){\n"
                + "ArbolAnalisis";
        for (int i= maxSimbolsRight-1;i>0;i-- ){
            ret+=" hijo"+i+",";
        }
        ret+= " hijo0;\n\n"
                + "if (!reglasConstruidas)\n"
                + "  construirReglas();\n\n"
                + printAccionesSemanticas( project,sh)
                +"\nreturn true;}}";
        return ret;
    }
    
    private String getImports(){
        String ret="";
        ArrayList<String> imports = MainWindow.getWindow().getGCMenu().getImports().saveToArray();
        for (String s : imports){
            ret+="import "+ s + ";\n";
        }
        return ret;
    }
    
    private String printRules(SyntaxHolder sh){
        String ret = "\n";
        int j=0;
        for(int i = sh.RulesSize();i>0;i--){

            ret += "private static Regla r"+j+";\n";
            j++;
        }
        ret+="\n";
        return ret;
    }
    
    private String printConstruirReglas(SyntaxHolder sh){
        String ret="\n";
        int numReglas= sh.RulesSize();
        maxSimbolsRight=0;
        for(int i=0;i<numReglas;i++)
        {
            SyntaxRule r= sh.GetRule(i);
            ret+=     "parteIzquierda = new NoTerminal(\""+r.GetLeftPartSymbol().toString()+"\");\n"
                    + "parteDerecha   = new CadenaSimbolos();\n";
            int rightPartSize= r.RightPartSize();
            for (int j = 0; j<rightPartSize;j++){
                SyntaxSymbol ss=r.GetRightPartSymbol(j);
                ret+="parteDerecha.anadirSimbolo(new ";
                ret+=ss.Terminal() ? "Terminal" :"NoTerminal";
                ret+="(\""+ss.toString()+"\"));\n";
                
            }
            if(rightPartSize>maxSimbolsRight)
                maxSimbolsRight= rightPartSize;
            ret+="r"+ i + "= new Regla(parteIzquierda,parteDerecha);\n\n";
        }
        return ret;
    }

    private String printAccionesSemanticas(Project project, SyntaxHolder sh) throws CompilerException {
        String ret="";
        SyntaxRule sr;
        int numReglas= sh.RulesSize();
        int lastLine=0;
        int lastHeader =0;
        int previusNonTerminal=-1;
        SyntaxHeader head=project.getHeaders()[lastHeader];
        SyntaxLine sl;
        JFlexRenamer jfr= new JFlexRenamer(null);
        /**Bucle check code aheard*/
        BucleChecker bc = new BucleChecker();
        bc.setAxiom(sh.GetAxiom().toString());
        ruleBucleChecker rbc;
        SymbolsStorage ss=SymbolsStorage.getStorage();
        for( Map.Entry<String, SymbolsStorage.SymbolInfo> entry: ss.storage.entrySet()){
            bc.addSymbol(entry.getKey(), entry.getValue().attributes.size(),entry.getValue().terminal);
        }
        /**Bucle check code ended*/
        for(int i=0;i<numReglas;i++){
            
            sr=sh.GetRule(i);
            rbc = new ruleBucleChecker(sr,bc);//Estabas aqui
            jfr.setRuleBucleChecker(rbc);
            if(lastLine==head.getLineNumber()){
                lastHeader++;
                lastLine=0;
                head=project.getHeaders()[lastHeader];
            }
            sl=head.getLine(lastLine);
            lastLine++;
            ret+="if(regla.equals(r"+i+")){\n";
            previusNonTerminal=-1;

            for(int j = sr.RightPartSize()-1; j>=0;j--){
                if(previusNonTerminal==-1&&!sr.GetRightPartSymbol(j).Terminal())
                    previusNonTerminal=j;
                ret+="hijo"+j+" = (ArbolAnalisis)arbol.hijos.get("+j+");\n";

            }
            
            ret+="if (atributosSintetizados.intValue() == 1 ){\n";
            ret+=" arbol.atributos.set(1,new GramaticaChristiansen((GramaticaChristiansen)";
            rbc.setAttributeDetectedInternal(sr.RightPartSize(), 1);
            if(previusNonTerminal==-1){
                ret+="arbol.atributos.get(0)));\n";
                rbc.getAttributeDetectedInternal(sr.RightPartSize(), 0);
            }
            else{
                ret+="hijo"+previusNonTerminal+".atributos.get(1)));\n";
                rbc.getAttributeDetected(sr.RightPartSize(),previusNonTerminal,0);
            }
                
            JavaCode jc= sl.getCode(sr.RightPartSize());
            if(jc!=null)
            {jfr.reset(new StringReader(jc.getCode()),sh, sr,sr.RightPartSize());
                jfr.yybegin(YYINITIAL);
                try {
                    jfr.yylex();
                } catch (IOException ex) {
                    Logger.getLogger(SemanticsCompiler.class.getName()).log(Level.SEVERE, null, ex);
                }
                ret+=jfr.toString()+"\n";
            }
            
            ret+="}\n";
            for(int k = sr.RightPartSize()-1;k>0; k--){
                ret+="if (atributosHeredadosHermanos.intValue() == "+k+" ){\n";
                if(!sr.GetRightPartSymbol(k).Terminal()){
                    for (int l=previusNonTerminal-1;l>=0;l--){
                        if(!sr.GetRightPartSymbol(l).Terminal()){
                            previusNonTerminal=l;
                            break;
                        }
                    }
                    ret+="hijo"+k+".atributos.set(0,new GramaticaChristiansen((GramaticaChristiansen)";
                    rbc.setAttributeDetected(k,k,0);
                    if(previusNonTerminal==-1){
                        ret+="arbol.atributos.get(0)));\n";                    
                        rbc.getAttributeDetectedInternal(k,0);
                    }
                    else{
                        ret+="hijo"+previusNonTerminal+".atributos.get(1)));\n";
                        rbc.getAttributeDetected(k,previusNonTerminal,1);
                    }
                    
                }
                jc= sl.getCode(k);
                if(jc!=null){
                    jfr.reset(new StringReader(jc.getCode()),sh, sr,k);
                    jfr.yybegin(YYINITIAL);
                    try {
                        jfr.yylex();
                    } catch (IOException ex) {
                        Logger.getLogger(SemanticsCompiler.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    ret+=jfr.toString()+"\n";
                }

                ret+="}\n";
            }
            
                 {
                int k=0;
                ret+="if (atributosHeredadosPadre.intValue() == 1 ){\n";
                SyntaxSymbol hijo;
                SymbolsStorage.SymbolInfo info;
                info = SymbolsStorage.getStorage().getInfo(sr.GetLeftPartSymbol().toString());
                ret+="for (int __i=arbol.atributos.size();__i<"+info.attributes.size()+";__i++){\n"+
                    "arbol.atributos.add(null);\n"+
                "}\n";
                
                for(int l = sr.RightPartSize()-1; l>=0;l--){
                    hijo=sr.GetRightPartSymbol(l);
                    info = SymbolsStorage.getStorage().getInfo(hijo.toString());
                    for(int m=info.attributes.size();m>0;m--){
                        ret+="hijo"+l+".attributos.add(null);\n";
                }}
                if(!sr.GetRightPartSymbol(k).Terminal()){
                    for (int l=previusNonTerminal-1;l>=0;l--){
                        if(!sr.GetRightPartSymbol(l).Terminal()){
                            previusNonTerminal=l;
                            break;
                        }
                    }
                    ret+="hijo"+k+".atributos.set(0,new GramaticaChristiansen((GramaticaChristiansen)arbol.atributos.get(0)));\n";
                    rbc.setAttributeDetected(0,0,0);
                    rbc.getAttributeDetectedInternal(0, 0);
                }
                jc= sl.getCode(k);
                if(jc!=null){
                    jfr.reset(new StringReader(jc.getCode()),sh, sr,0);
                    jfr.yybegin(YYINITIAL);
                    try {
                        jfr.yylex();
                    } catch (IOException ex) {
                        Logger.getLogger(SemanticsCompiler.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    ret+=jfr.toString()+"\n";
                }

                ret+="}\n";
            }
            
            ret+="}\n";
            rbc.addDataToBucleChecker();
        }
        if(bc.check()){
            JPConsole.addText("WARNING: Posible cycle detected in the attributes");
        }
        return ret;
    }


    void CompilateProjectToFile(Project project, SyntaxHolder sh, String path) throws FileNotFoundException, IOException, CompilerException {
        
        String s= CompilateProject(project,sh);
        Writer w = new BufferedWriter(new  OutputStreamWriter(new FileOutputStream(path+className+"."+FILE_EXTENSION)));
        w.write(s);
        w.close();
    }
    
}
