/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Compiler;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author ricardo
 */
public class BucleChecker {
    private HashMap<String,ArrayList> symbols;
    private boolean detected;
    private String axioma;
    
    protected HashMap<String,ArrayList> getSymbols(){
        return symbols;
    }
    
    public BucleChecker(){
        detected = false;
        symbols = new HashMap();
    }
    
    public void addSymbol(String name, int numberAtributes,boolean terminal){
        ArrayList<atributes> ats;
        atributes at;
        if(numberAtributes>0){
            ats= new ArrayList(numberAtributes);
            for(int i = 0;i<numberAtributes;i++){
                at= new atributes(terminal);
                ats.add(at);
            }
            symbols.put(name, ats);
            
        }
    }
    
    public void internalUnSet(String symbolName, int atribute){
        ArrayList<atributes> ats=symbols.get(symbolName);
        ats.get(atribute).iSet=false;
    }
    
    public void externalUnSet(String symbolName, int atribute){
        ArrayList<atributes> ats=symbols.get(symbolName);
        ats.get(atribute).eSet=false;        
    }
        
    public void internalGet(String symbolName, int atribute){
        ArrayList<atributes> ats=symbols.get(symbolName);
        ats.get(atribute).iGet=true;        
    }
            
    public void externalGet(String symbolName, int atribute){
        ArrayList<atributes> ats=symbols.get(symbolName);
        ats.get(atribute).eGet=true;        
    }
    
    public void foundBucle(){
        detected=true;
    }
    
    public boolean check(){
        if(detected)
            return true;
        for (String symbol:symbols.keySet()){
            if(symbol.equals(axioma)){
                boolean first = true;
                for(atributes at:((ArrayList <atributes>)symbols.get(symbol))){
                    if(first){
                        first=false;
                        if(!at.eSet&&(at.iGet||(!at.iSet&&at.eGet)))
                            return true;
                    }
                    else if((at.iGet||(!at.eSet&&!at.iSet&&at.eGet)))
                        return true;
                }
            }
            else{
                for(atributes at:((ArrayList <atributes>)symbols.get(symbol))){
                    if(!at.eSet&&(at.iGet||(!at.iSet&&at.eGet)))
                        return true;
                }
            }
        }
        return false;
    }

    void setAxiom(String axioma) {
        this.axioma=axioma;
    }
    
    public class atributes{
        public boolean iSet; //internal Set
        public boolean iGet; //internal Get 
        public boolean eSet; //external Set
        public boolean eGet; //external Get
        
        public atributes(){
            iSet=true;
            iGet=false;
            eSet=true;
            eGet=false;
        }

        private atributes(boolean terminal) {
            iSet=!terminal;
            iGet=false;
            eSet=true;
            eGet=false;      }
    }
    
}
