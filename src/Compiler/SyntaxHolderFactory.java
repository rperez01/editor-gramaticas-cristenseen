/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Compiler;

import ExternalComunication.SyntaxHolder;
import ExternalComunication.SyntaxRule;
import ExternalComunication.SyntaxSymbolNonTerminal;
import ExternalComunication.SyntaxSymbolTerminal;
import InternalLogic.Project;
import InternalLogic.SyntaxHeader;
import ide.gc.globalvalues.SymbolsStorage;

/**
 * Creates a syntax holder using the applicartion data
 * @author Ricardo Pérez Cortés
 */
public class SyntaxHolderFactory {
    public SyntaxHolderFactory(){
    }
    
    public SyntaxHolder createHolder(Project p){
        SymbolsStorage s=
        SymbolsStorage.getStorage();
        SyntaxHolder sh=new SyntaxHolder();
        SyntaxRule sr;
        String leftPart;
        for(String str :s.storage.keySet()){
            if(((SymbolsStorage.SymbolInfo)s.storage.get(str)).terminal){
                sh.AddSymbol(new SyntaxSymbolTerminal(str));
            }
            else{
                sh.AddSymbol(new SyntaxSymbolNonTerminal(str));
            }
        }
        
        SyntaxHeader[] h =p.getHeaders();
        for(SyntaxHeader head : h){
            if(head.getHeader().length()==0){
                return null;
            }

            leftPart=head.getHeader().substring(0,head.getHeader().length()-1).split(" ")[0].trim();
            SyntaxSymbolNonTerminal leftPartSymbol = new SyntaxSymbolNonTerminal(leftPart);
            for(int i=0; i<head.getLineNumber() ;i++){
                sr=new SyntaxRule(leftPartSymbol);
                String[] line = head.getLine(i).getLine().trim().split(" ");
                for(String aux : line){
                    
                    SyntaxSymbolTerminal opt1;
                    SyntaxSymbolNonTerminal opt2;
                    aux=aux.trim();
                    opt1 = new SyntaxSymbolTerminal(aux);
                    opt2 = new SyntaxSymbolNonTerminal (aux);
                    if(sh.ContainsSymbol(opt1))
                        sr.AddRightPartSymbol(opt1);
                    else
                        sr.AddRightPartSymbol(opt2);
                }
                sh.AddRule(sr);
            }
        }
        if(sh.CheckConsistency())
            return sh;
        else
            return null;
    }
}
