/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Compiler;

import java.util.LinkedList;

/**
 *
 * @author Ricardo
 */
public class ParentesisChecker {
    
    private LinkedList<Integer> functionPos ;
    private LinkedList<SimpleFunction> functions;
    private int parentesisPosition;
    
    public ParentesisChecker(){
        functionPos= new LinkedList();
        functions= new LinkedList();
        parentesisPosition= 0;
    }
    
    public void addOpenParentesis(){
        parentesisPosition++;
    }
    
    public void addClosingParentesis(){
        Integer aux = new Integer(parentesisPosition);
        while(aux.equals(functionPos.peekLast())){
            functionPos.getLast();
            functions.getLast().function();
        }
        parentesisPosition--;
        
    }
    
    public void setFunctionWhenClosing(SimpleFunction sf){
        functionPos.addLast(parentesisPosition);
        functions.addLast(sf);
    }
}
