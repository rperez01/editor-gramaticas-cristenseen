/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Compiler;

import ExternalComunication.SyntaxHolder;
import InternalLogic.Project;
import InternalLogic.SaveLoader;
import ide.gc.sintaxLogic.SyntaxMarker;
import ide.gc.ui.JPConsole;
import java.awt.event.ActionEvent;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;

/**
 * Class that activates itself when presed the compile button
 * @author ricardo
 */
public class CompileMain extends AbstractAction {
    
    private SyntaxHolderFactory shf;
    private SemanticsCompiler sc;
    private static final  String OUT_PATH="../Compilated/";
    private static final  String SYNTAX_FILE="Syntax.json";
    private static final  String SEMANTIC_FILE="Semantics";
    
    
    public  CompileMain(){
        super("Compile");
        shf=new  SyntaxHolderFactory();
        sc= new SemanticsCompiler(SaveLoader.GetProjectName()+SEMANTIC_FILE);
    }
    
    @Override
    public void actionPerformed(ActionEvent ae) {
        SyntaxHolder sh=shf.createHolder(SyntaxMarker.getInstance().getProject());
        JPConsole.clear();
        if(sh==null)
        {
            JPConsole.addText("Syntax consistency compromised.\n"
                    + " Remember to declare every Symbol as a terminal or non terminal in the global values tab");
        }
        else{
            try {
                sh.SaveToFile(OUT_PATH+SaveLoader.GetProjectName()+SYNTAX_FILE);
                JPConsole.addText("Syntax compilation successful");
                sc.SetClassName(SaveLoader.GetProjectName()+SEMANTIC_FILE);
                sc.CompilateProjectToFile(SyntaxMarker.getInstance().getProject(), sh,OUT_PATH);
                JPConsole.addText("Semantics compilation successful");
            } catch (IOException ex) {
                JPConsole.addText("Error opening output file");
                Logger.getLogger(CompileMain.class.getName()).log(Level.SEVERE, null, ex);
            } catch (CompilerException ex) {
                JPConsole.addText(ex.getMessage());
            }
        }
        
    }
    
}
