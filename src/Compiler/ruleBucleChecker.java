/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Compiler;

import ExternalComunication.SyntaxRule;
import ide.gc.globalvalues.SymbolsStorage;
import ide.gc.globalvalues.SymbolsStorage.SymbolInfo;
import java.util.TreeMap;


/**
 *
 * @author ricardo
 */
public class ruleBucleChecker {
    private SyntaxRule rule;
    private TreeMap<Integer,Container> [] instants;
    private final BucleChecker bc;
    private TreeMap<Integer,ContainerInternal>  internal;
    //private TreeMap<Integer,Container> attributesTouched;
    public enum way{Untouched,Setted,Getted};
    
    public ruleBucleChecker(SyntaxRule rule,BucleChecker checker){
        this.rule= rule;
        instants = new TreeMap[rule.RightPartSize()];
        for(int i = 0;i<instants.length ;i++){
            instants[i]=new TreeMap();
        }
        bc= checker;
        internal = new TreeMap(); 
    }
    
    public void setAttributeDetected(int instant,int atributeInstant, int atributeNumber){
       //Key key=new Key(atributeInstant,atributeNumber);
       Container c = new Container();
       Container aux;
       TreeMap<Integer,Container> map = instants[atributeInstant];
       if(atributeInstant<=instant){
           c.after=way.Untouched;
           if(map.containsKey(atributeNumber))
           {
               aux=map.get(atributeNumber);
               if(aux.lastInstant==instant)
                   return;
               c.after=aux.after;
           }
           c.before=way.Setted;
           
       }
       else{
           c.before=way.Untouched;
           if(map.containsKey(atributeNumber))
           {
               aux=map.get(atributeNumber);
               if(aux.lastInstant==instant)
                   return;
               c.before=aux.before;
           }
           c.after=way.Setted;
       }
       c.lastInstant=instant;
       map.put(atributeNumber,c);
    }
    public void getAttributeDetected(int instant,int atributeInstant, int atributeNumber){
       Container c = new Container();
       Container aux;
       TreeMap<Integer,Container> map = instants[atributeInstant];
       if(atributeInstant<=instant){
           c.after=way.Untouched;
           if(map.containsKey(atributeNumber))
           {
               aux=map.get(atributeNumber);
               if(aux.lastInstant==instant)
                   return;
               c.after=aux.after;
           }
           c.before=way.Getted;
           c.lastInstant=instant;
       }
       else{
           c.before=way.Untouched;
           if(map.containsKey(atributeNumber))
           {
               aux=map.get(atributeNumber);
               if(aux.lastInstant==instant)
                   return;
               c.before=aux.before;
           }
           c.after=way.Getted;
           c.lastInstant=instant;
       }
       map.put(atributeNumber,c);
    }
    
    public void setAttributeDetectedInternal(int instant,int atributeNumber){
        ContainerInternal c;
        if(internal.containsKey(atributeNumber)){
            c=internal.get(atributeNumber);
            if(c.lastInstant>instant)
                c.uganda=way.Setted;
            c.lastInstant=instant;
        }
        else{
            c=new ContainerInternal();
            c.lastInstant=instant;
            c.uganda=way.Setted;
            internal.put(atributeNumber, c);
        }
    }
    
    public void getAttributeDetectedInternal(int instant,int atributeNumber){
        ContainerInternal c;
        if(internal.containsKey(atributeNumber)){
            c=internal.get(atributeNumber);
            if(c.lastInstant>instant){
                c.uganda=way.Getted;
                c.lastInstant=instant;
            }
        }
        else{
            c=new ContainerInternal();
            c.lastInstant=instant;
            c.uganda=way.Getted;
            internal.put(atributeNumber, c);
        }
    }
    
    public void addDataToBucleChecker(){
        String symbol;
        SymbolInfo si;
        SymbolsStorage ss=SymbolsStorage.getStorage();
        for(int i=0;i<instants.length;i++){
            symbol=rule.GetRightPartSymbol(i).toString();
            si = ss.getInfo(symbol);
            if(si==null)
                continue;
            for(int j=0;j<si.attributes.size();j++){
                if(instants[i].containsKey(j)){
                    Container uganda= instants[i].get(j);
                    if(uganda.before==way.Getted){
                        bc.foundBucle();
                    }
                    else if(uganda.before==way.Untouched){
                        bc.externalUnSet(symbol, j);
                    }
                    if(uganda.after==way.Getted){
                        bc.externalGet(symbol, j);
                    }   
                }
                else{
                    bc.externalUnSet(symbol,j);
                }
            }
  
            
            
        }
        symbol=rule.GetLeftPartSymbol().toString();
        si = ss.getInfo(symbol);
        ContainerInternal defaultC= new ContainerInternal();
        ContainerInternal ci;
        defaultC.uganda=way.Untouched;
        
        for(int j =0;j<si.attributes.size();j++){
            ci=internal.getOrDefault(j, defaultC);
            if(ci.uganda!=way.Setted)
                bc.internalUnSet(symbol, j);
            if(ci.uganda==way.Getted)
                bc.internalGet(symbol, j);
            
        }
    }
  /*  private class Key {
        public final int atributeInstant;
        public final int atributeNumber;
        
        public Key(int atributeInstant, int atributeNumber){
            this.atributeInstant=atributeInstant;
            this.atributeNumber=atributeNumber;
        }
        
        @Override
        public boolean equals(Object obj)
        {
            Key other;
            if(obj instanceof Key)
            {
                other =((Key) obj);
                return atributeNumber==other.atributeNumber&&atributeInstant==other.atributeInstant;
            }
            return false;
        }
        
        @Override
        public int hashCode()
        {
            return atributeInstant*16+atributeNumber;
        }
    }*/
    private class Container{
        way before;
        way after;
        int lastInstant;
    }
    
    private class ContainerInternal{
        way uganda;
        int lastInstant;
    }
}
