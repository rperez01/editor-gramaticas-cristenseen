/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Compiler;

import ExternalComunication.SyntaxRule;

/**
 * Exception throwed when trying to compile
 * @author Ricardo Pérez Cortés
 */
public class CompilerException extends Exception {
    public CompilerException(String message, int column,int instant , SyntaxRule sr){
        super("Error:"+message+", at rule \""+sr.toString()+"\", instant:"+ instant+",column:"+column);
    }
}
