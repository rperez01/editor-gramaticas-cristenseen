/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TextChunkDividers;

/**
 * Marca vacia usada como marca auxiliar
 * @author Ricardo Pérez Cortés
 */
public class EmptyMark extends AbstractMark{

    @Override
    public boolean isStartingMark() {
        return false;

    }

    @Override
    public boolean isFinishMark() {
        return true;
    }

    @Override
    public int getMarkLength() {
        return -1;
    }
        
}
