/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TextChunkDividers;

/**
 *
 * @author usuario
 */
public abstract class AbstractMark implements PrivateMark{
    protected int position;
    
    public AbstractMark(){
        position=0;
    }
    
    @Override
    public void setPosition(int position) {
        this.position=position;
    }

    @Override
    public int getPosition() {
        return position;
    }

    @Override
    public boolean shouldStay(PrivateMark beforeMark){
        return true;
    }

    @Override
    public abstract boolean isStartingMark();

    @Override
    public abstract boolean isFinishMark();
    
    @Override
    public abstract int getMarkLength();
    
}
