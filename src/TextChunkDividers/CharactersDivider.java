/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TextChunkDividers;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

/**
 * Class that uses TextChunkDivider to divide a text using only one character.
 * @author Ricardo Pérez Cortés
 */
public class CharactersDivider implements DocumentListener{
    
    private Document doc;
    public TextChunkDivider tcd;
    private char []charDivider;
    public CharactersDivider(Document doc,char[] character){
        doc.addDocumentListener(this);
        this.doc =doc;
        charDivider= character;
        tcd=new TextChunkDivider();
        try {
            AnaliceString(doc.getText(0, doc.getLength()),0);
        } catch (BadLocationException ex) {
            Logger.getLogger(CharactersDivider.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void AnaliceString(String s, int offset){
        char[] charArray;
        int j;
        charArray=s.toCharArray();
        for (int i =0;i<charArray.length-charDivider.length+1;i++){
            for(j=0;j<charDivider.length;j++)
                if(charArray[j+i]!=charDivider[j])
                    break;
            if(j==charDivider.length)
                tcd.insertMark(i+offset, new CharacterMark());
                
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        tcd.insertUpdate(e);
        try {
            AnaliceString(
                    doc.getText(e.getOffset(), e.getLength())
                    ,e.getOffset());
        } catch (BadLocationException ex) {
            Logger.getLogger(CharactersDivider.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        tcd.removeUpdate(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        tcd.changedUpdate(e);
    }
    
    
            
    private class CharacterMark extends AbstractMark{

        @Override
        public boolean isStartingMark() {
            return true; //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean isFinishMark() {
            return true; //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public int getMarkLength() {
            return 1; //To change body of generated methods, choose Tools | Templates.
        }

        
    }
}
