/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TextChunkDividers;

/**
 *
 * @author Ricardo Perez Cortes
 */
public interface Mark{
/**
     * Determina si una marca debería estar tras otra marca
     * @param beforeMark marca anterior
     */
    public boolean shouldStay(PrivateMark beforeMark);
    /**
     * Determina si la marca debe empezar un bloque
     */
    public boolean isStartingMark();
     /**
     * Determina si la palabra debe finalizar un boque
     */
    public boolean isFinishMark();
    /**
     * Devuelve la longitud de la marca
     * @return la longitud de la marca
     */
    public int getMarkLength();
}
