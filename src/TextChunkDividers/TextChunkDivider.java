/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TextChunkDividers;

import java.util.ArrayList;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 * Lleva la cuenta y todos los comentarios de un documento.
 * Se encarga automaticamente de la eliminación de estos, pero necesita que 
 * le digan cuando uno ha sido abierto o cerrado.
 * (Esta clase se ha añadir como listener al documento portador de las marcas. las marcas han de añadirse posteriormente)
 * @author Ricardo Perez Cortes
 */
public class TextChunkDivider implements DocumentListener{
    private ArrayList <PrivateMark> marks;
    private int closestMark;
    private int realPosClosestMark;
    private ArrayList <MarkListener> ml;
    
    public TextChunkDivider(){
        PrivateMark empty;
        marks=new ArrayList ();
        ml = new ArrayList();
        empty=new EmptyMark();
        empty.setPosition(0);
        marks.add(empty);
        closestMark=0;
        realPosClosestMark=0;
    }
    /**
     * Adds a markListener at this text chunk divider
     * @param markListener the markListener that is gonna be added
     */
    public void addListener(MarkListener markListener){
        ml.add(markListener);
    }
    /**
     * Removes a markListener at this text chunk divider
     * @param markListener The markListener that is gonna be removed
     */
    public void removeListener(MarkListener markListener){
        ml.remove(markListener);
    }
    /**
     * Busca la marca con la posición pos o su la más cercana con posición cercana
     * @param pos la posición a la que buscar la marca
     * @return el indice de la marca más cercana
     */
    public int getNearestMarkIndex(int pos){
        int realPos, index,aux;
        PrivateMark mark=null;
        index=closestMark;
        mark=marks.get(index);
        realPos=realPosClosestMark;
        if(pos<0)
            pos=0;
        if(realPos>pos){
            realPos-=mark.getPosition();
            index--;
            while(index>0&&realPos>pos){
                mark=marks.get(index);
                realPos-=mark.getPosition();
                index--;
            }
        }            
        else {
            while(index+1<marks.size()&&
                    (aux=
                    realPos+marks.get(index+1).getPosition())
                    <=pos){
                realPos=aux;
                index++;
            }
        }
        realPosClosestMark=realPos;
        closestMark=index;
        return index;
    }
    
    private void removeRangeOfMarks(int initMark, int endMark){
        if(initMark<=endMark){
            PrivateMark nextMark;
            int totalPosition=0;
            if(marks.size()>endMark+1){
                nextMark=marks.get(endMark+1);
                for(int i= endMark;i>=initMark;i-- ){
                    totalPosition+=marks.get(i).getPosition();
                    for(MarkListener rl: ml){
                        rl.MarkRemoved(i);
                    }
                    marks.remove(i);
                }
                nextMark.setPosition(nextMark.getPosition()+totalPosition);
                if(!nextMark.shouldStay(marks.get(initMark-1)))
                    removeMark(initMark);
            }
            else
                for(int i= endMark;i>=initMark;i-- ){
                    for(MarkListener rl: ml){
                        rl.MarkRemoved(i);
                    }
                    marks.remove(i);
                }
        }
    }
    private void removeMark(int mark){
        PrivateMark nextMark;
        for(MarkListener rl: ml){
            rl.MarkRemoved(mark);
        }
        if(marks.size()>mark+1){
            nextMark=marks.get(mark+1);
            nextMark.setPosition(nextMark.getPosition()+marks.get(mark).getPosition());
            marks.remove(mark);
            if(!nextMark.shouldStay(marks.get(mark-1)))
                removeMark(mark);
        }
        else
            marks.remove(mark);

    }
    /**
     * Actualiza los offset de las marcas cuando se añaden carácteres.
     * También elimina una marca si se añaden caracteres entre la marca existente
     * @param e el evento
     */
    @Override
    public void insertUpdate(DocumentEvent e) {
        int start= e.getOffset();
        int length = e.getLength();
        PrivateMark mark1;
        int index1;
                if(start!=0)
                    index1=getNearestMarkIndex(start-1);
                else
                    index1=getNearestMarkIndex(start);
       mark1=marks.get(index1);
        if(start==1&&marks.size()>index1+1&&marks.get(index1+1).getPosition()==0){
            mark1=marks.get(index1+1);
        }
        if(realPosClosestMark+mark1.getMarkLength()-1>start){
            realPosClosestMark-=mark1.getPosition();
            closestMark=index1-1;
            removeMark(index1);
            index1--;

        }
        if(marks.size()>index1+1){
            mark1=marks.get(index1+1);
            mark1.setPosition(mark1.getPosition()+length);
        }
    }
    
    /**
     * Given an index of a mark returns its postion
     * @param index the index of the mark
     * @return the position of the mark
     */
    public int indexToPosition(int index){
        PrivateMark m;
        if(index<=0){
            closestMark=0;
            realPosClosestMark=0;
            return 0;
        }
        else if (index>=marks.size())
            index=marks.size()-1;
        if(closestMark<index){
            while(closestMark<index){
                closestMark++;
                m=marks.get(closestMark);
                realPosClosestMark+=m.getPosition();
            }
        }
        else{
            while(closestMark>index){
                m=marks.get(closestMark);
                closestMark--;
                realPosClosestMark-= m.getPosition();
            }
        }
        return realPosClosestMark;
    }
    
    
    /**
     * Actualiza los caracteres necesarios cuando se eliminan caracteres
     * También comprueba que no se haya removido una marca en cuyo caso actualiza
     * el resto
     * @param e el evento
     */
    @Override
    public void removeUpdate(DocumentEvent e) {
        int start= e.getOffset();
        int length = e.getLength();
        int index1,index2;
        PrivateMark mark1,mark2=null;
        if(length==1){
            index1=getNearestMarkIndex(start);
            mark1=marks.get(index1);
            if(realPosClosestMark+mark1.getMarkLength()>start){
                closestMark=index1-1;
                realPosClosestMark-=mark1.getPosition();
                removeMark(index1);
                index1--;

            }
            if(marks.size()>index1+1){   
                    mark2=marks.get(index1+1);
                    mark2.setPosition(mark2.getPosition()-length);
            }
        }
        else{
            index2=getNearestMarkIndex(start+length);
            index1=getNearestMarkIndex(start);
            mark1=marks.get(index1);
            if(realPosClosestMark+mark1.getMarkLength()>start){
                closestMark=index1-1;
                realPosClosestMark-=mark1.getPosition();
                removeRangeOfMarks(index1,index2);
                index1--;
            }
            else
            {
                removeRangeOfMarks(index1+1,index2);
            }
            if(marks.size()>index1+1){
                    mark2=marks.get(index1+1);
                    mark2.setPosition(mark2.getPosition()-length);
            }
        }
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
    }
    
    /**
     * Inserta una marca en la posición pos
     * @param pos  la posicion en la que introducirla
     * @param mark la marca a introducir
     */
    public void insertMark(int pos, PrivateMark mark) {
        PrivateMark mark1,mark2=null;
        int distance;
        mark1=marks.get(getNearestMarkIndex(pos));
        if(pos>realPosClosestMark+mark1.getMarkLength()-1){
            
            if(!mark.shouldStay(mark1))
                return;
            distance=pos-realPosClosestMark;
            if(marks.size()>closestMark+1){
                mark2=marks.get(closestMark+1);
                mark2.setPosition(mark2.getPosition()-distance);
            }
            marks.add(closestMark+1, mark);
            mark.setPosition(distance);
            for(MarkListener rl: ml){
                rl.MarkAdded(closestMark+1);
            }
            if(mark2!=null){
                
                if(!mark2.shouldStay(mark))
                    removeMark(closestMark+2);
            }

        }
    }
    /**
     * Devuelve la posicion de la marca anterior a una posicion
     * @param pos la posicion del documento
     * @return la posicion de la marca previa
     */
    public int getPreviousMarkPosition(int pos){
        if(pos<0)
            pos=0;
        getNearestMarkIndex(pos);
        return realPosClosestMark;
    }
    /**
     * Devuelve la posicion de la marca siguiente  a una posicion
     * @param pos la posicion del documento
     * @return la posicion de la marca siguiente
     */
    public int getNextMarkPosition(int pos){
        PrivateMark mark;
        getNearestMarkIndex(pos);
        if(marks.size()>closestMark+1){
            mark=marks.get(closestMark+1);
            return realPosClosestMark+mark.getPosition();
        }
        else
            return -1;
    }
    /**
     * Devuelve  la marca anterior  a una posicion dada
     * @param pos la posicion del documento
     * @return la marca anterior
     */
    public Mark getPreviousMark(int pos){
        getNearestMarkIndex(pos);
        return marks.get(closestMark);
    }
    /**
     * Devuelve la siguiente marca a una posicion dada
     * @param pos la posicion del documento
     * @return la marca siguiente
     */
    public Mark getNextMark(int pos){
        Mark mark;
        getNearestMarkIndex(pos);
        if(marks.size()>closestMark+1){
            mark=marks.get(closestMark+1);
            return mark;
        }
        else
            return null;
    }
    /**Compruebas si una posicion esta dentro de un bloque
     * @param pos  la posicion del documento
     * @return true si esta dentro de un bloque false de otro modo
     */
    public boolean isInside(int pos){
        Mark mark1;//,mark2;
        
        getNearestMarkIndex(pos-1);
        //if(marks.size()>closestMark+1){
            mark1=marks.get(closestMark);
            //mark2=marks.get(closestMark+1);
            return mark1.isStartingMark();//&&mark2.isFinishMark();
        /*}
        else
            return false;*/
    }
    /** Elimina todos los datos de la estructura para nuevo uso
     * 
     */
    public void clear(){
        PrivateMark empty;
        empty=marks.get(0);
        marks.clear();
        marks.add(empty);
        closestMark=0;
        realPosClosestMark=0;
    }
    

}
