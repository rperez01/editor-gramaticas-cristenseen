/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TextChunkDividers;

/**
 * Estructura de la marca
 * position: posicion relativa a la marca anterior
 * length: longitud de la marca
 * @author Ricardo Perez Cortes
 */
public interface PrivateMark extends Mark{
        /**
     * Actualiza la posicion de la marca 
     * @param position la nueva posicion de la marca
     */
     void setPosition(int position);
    /**
     * Devuelve la posicion de la palabra
     * @return la posicion de la palabra
     */
     int getPosition();
    
}
