/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TextChunkDividers;

/**
 * Marca de comentarios necesaria para indicar si se
 * esta dentro de un bloque de comentarios
 * @author Ricardo Pérez Cortés
 */
public class MultiComentatorMark extends AbstractMark {

    @Override
    public boolean isStartingMark() {
                return type==TYPE.OPENING; 
    }

    @Override
    public boolean isFinishMark() {
        return type==TYPE.CLOSING;
    }
    
    @Override
    public boolean shouldStay(PrivateMark beforeMark){
        return isFinishMark()||beforeMark.isFinishMark();            
    }

    @Override
    public int getMarkLength() {
        return 2;
    }
    
    public enum TYPE{
        CLOSING, OPENING;
    }
    private final TYPE type;
    
    public MultiComentatorMark(TYPE type){
        super();
        this.type=type;
    }
    
    
    public TYPE getType(){
        return type;
    }
    
    
}
