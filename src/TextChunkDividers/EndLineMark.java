/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TextChunkDividers;

/**
 * Marca que indica el final de la linea
 * @author Ricardo
 */
public class EndLineMark extends AbstractMark{
    @Override
    public boolean isStartingMark() {
        return false;
    }

    @Override
    public boolean isFinishMark() {
        return true;
    }
    
    @Override
    public boolean shouldStay(PrivateMark beforeMark){
        return beforeMark.isFinishMark();            
    }

    @Override
    public int getMarkLength() {
        return 1;
    }
}
