/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TextChunkDividers;



/**
 * Designed to be called when some mark is added or removed from a a 
 * TextChunkDivider
 * @author Ricardo Pérez Cortés
 */
public interface MarkListener {
    /**
     * Called wjen a mark is removed
     * @param index the index of the removed mark
     */
    void MarkRemoved(int index);
    
    /**
     * Called when a mark is added
     * @param index the index of the added mark
     */
    void MarkAdded(int index);
}
