/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ide.gc.globalvalues;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */





/**
 *
 * @author Ricardo Pérez Cortés
 */
public class Precedence extends GlobalValuesPopUp{


    /**
    *Type of the precedence
    */

    public  enum Type{
            RIGHT, LEFT, NONASOC
        }
    private static final String MENU_NAME = "Precedences";
    private static final String NAME = "Name:";
    private static final String PRECEDENCE = "Precedence:";
    private static final String [] PRECEDENCELIST = {"Right","Left","Non Asoc"};
    private static ArrayList<PrecedenceInfo> info = new ArrayList();

    

    public Precedence(JFrame parent){
        super(parent,MENU_NAME);
        

    }
    
    protected void NewElement(JPanel container){
        JPanel aux;
        JButton button;
        JComboBox jcb;
        UpDownButton udb;
        int componentCount;
        aux= new JPanel();
        button =  new JButton("X");

        aux.add(button);
        udb=new UpDownButton(aux);
        button.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                UpDownButton udbd,udbu;
                if(udb.downPanel!=null){
                    udbd=(UpDownButton)udb.downPanel.getComponent(1);
                    udbd.upPanel=udb.upPanel;
                }
                if(udb.upPanel!=null){
                    udbu=(UpDownButton)udb.upPanel.getComponent(1);
                    udbu.downPanel=udb.downPanel;
                }
                container.remove(aux);
                container.updateUI();
            }
        });
        aux.add(udb);//TO DO
        aux.add(new JLabel(PRECEDENCE));
        jcb=new JComboBox(PRECEDENCELIST);
        componentCount=container.getComponentCount();
        if(componentCount>1){
            jcb.setSelectedIndex(
            ((JComboBox)
            ((JPanel)container.getComponent(componentCount-2)).
                    getComponent(3)).
                    getSelectedIndex());
        }
        aux.add(jcb);
        
        aux.add(new JLabel(NAME));
        aux.add(new JTextField(5));
        int cc = container.getComponentCount();
        if(cc>1){
            UpDownButton udb2= ((UpDownButton)((JPanel)container.getComponent(cc-2)).getComponent(1));
            udb2.downPanel=aux;
            udb.upPanel=udb2.parent;
        }
        container.add(aux,cc-1);
        container.updateUI();
    }
    

    @Override
    protected void UpdateInfo(JPanel container) {
        info.clear();
        Component components[] =container.getComponents();
        JPanel jp;
        for(int i=0;i<components.length-1;i++){
            jp =  (JPanel) components[i];
            info.add(
            new PrecedenceInfo(
                ((JComboBox) jp.getComponent(3)).getSelectedIndex(),
                ((JTextField)jp.getComponent(5)).getText()
            ));
        }
    }

    @Override
    protected void LoadInfo(JPanel container) {
        for(PrecedenceInfo pi: info){
        JPanel aux;
        JButton button;
        JComboBox jcb;
        UpDownButton udb;
        aux= new JPanel();
        button =  new JButton("X");

        aux.add(button);
        udb = new UpDownButton(aux);
        aux.add(udb);//TO DO
        button.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                UpDownButton udbd,udbu;
                if(udb.downPanel!=null){
                    udbd=(UpDownButton)udb.downPanel.getComponent(1);
                    udbd.upPanel=udb.upPanel;
                }
                if(udb.upPanel!=null){
                    udbu=(UpDownButton)udb.upPanel.getComponent(1);
                    udbu.downPanel=udb.downPanel;
                }
                container.remove(aux);
                container.updateUI();
            }
        });
        aux.add(new JLabel(PRECEDENCE));
        jcb=new JComboBox(PRECEDENCELIST);
        jcb.setSelectedIndex(pi.precedence);
        aux.add(jcb);
        aux.add(new JLabel(NAME));
        aux.add(new JTextField(pi.name,5));
        int cc = container.getComponentCount();
        if(cc>1){
            UpDownButton udb2= ((UpDownButton)((JPanel)container.getComponent(cc-2)).getComponent(1));
            udb2.downPanel=aux;
            udb.upPanel=udb2.parent;
        }
        container.add(aux,cc-1);
        
        }
        container.updateUI();
    }
                
    
    public class PrecedenceInfo{


        public PrecedenceInfo(int precedence, String name){
            this.precedence=precedence;
            this.name=name;
        }
        
        public int precedence;
        public String name;
    }
    
    @Override
    public ArrayList<PrecedenceInfo> saveToArray(){
        return info;
    }
    
    
    @Override    
    public void loadFromArray(ArrayList array) {
       info = (ArrayList<PrecedenceInfo>) array;
       LoadInfo(super.scroll);
    }
    
    @Override
    public void Reset() {
        info.clear();
        LoadInfo(super.scroll);
    }
    
    
    
    private class UpDownButton extends JPanel{
        private JButton up;
        private JButton down;
        private JPanel upPanel;
        private JPanel downPanel;
        private JPanel parent;
        private UpDownButton(JPanel p){
            super();
            this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
            parent = p;
            upPanel=null;
            downPanel=null;
            up=new JButton("\u25B2");
            down= new JButton("\u25BC");
            this.add(up);
            this.add(down);
            up.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent e) {
                    if(upPanel!=null){
                        SwapContent(p,upPanel);
                    }
                }
                
            });
            down.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent e) {
                    if(downPanel!=null){
                        SwapContent(p,downPanel);
                    }
                }
                
            });
        }
        
        private void SwapContent(JPanel jpa1, JPanel jpa2){
            int precedence;
            String name;
            precedence=((JComboBox) jpa1.getComponent(3)).getSelectedIndex();
            name=((JTextField)jpa1.getComponent(5)).getText();
            ((JComboBox) jpa1.getComponent(3)).setSelectedIndex(((JComboBox) jpa2.getComponent(3)).getSelectedIndex());
            ((JTextField)jpa1.getComponent(5)).setText(((JTextField)jpa2.getComponent(5)).getText());
            ((JComboBox) jpa2.getComponent(3)).setSelectedIndex(precedence);
            ((JTextField)jpa2.getComponent(5)).setText(name);
            
        }
    }
    
}


