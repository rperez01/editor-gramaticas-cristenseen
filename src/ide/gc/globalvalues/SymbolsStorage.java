/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ide.gc.globalvalues;

import TextChunkDividers.MultiComentatorMark;
import TextChunkDividers.MultiComentatorMark.TYPE;
import TextChunkDividers.TextChunkDivider;
import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JEditorPane;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;


/**
 * It stores the Symbols in a cool way that is easy to extract for things that
 * Alfonso whants me to do
 * @author ricardo
 */
public class SymbolsStorage {
    public static MutableAttributeSet blueLetters;
    public  HashMap<String, SymbolInfo> storage;
    private ArrayList<ChangeListener<SymbolsStorage>> listeners;
    private ArrayList<SymbolNames.Info> symbolNamesInfo;
    private ArrayList<SymbolAttributes.Info>  symbolAttributesInfo;
    private static SymbolsStorage instance;
    
    public static SymbolsStorage getStorage(){
        if(instance == null){
            instance = new SymbolsStorage();
        }
        return instance;
        
    }
    
    private SymbolsStorage(){
        storage = new HashMap();
        listeners = new ArrayList();
        
        if(blueLetters==null){
            blueLetters= new SimpleAttributeSet();
            StyleConstants.setForeground(blueLetters, Color.BLUE);
        }
        symbolNamesInfo=null;
        symbolAttributesInfo=null;
    }
    public class SymbolInfo{
        public boolean terminal;

        public LinkedHashMap<String,String> attributes = new LinkedHashMap();
    }

    public SymbolInfo getInfo(String syntaxSymbol){
        return storage.get(syntaxSymbol);
    }
    
    public void addListener(ChangeListener<SymbolsStorage> listener){
        listeners.add(listener);
    }
    
    public void removeListener(ChangeListener<SymbolsStorage> listener){
        listeners.remove(listener);
    }
    
    private void StorageUpdate(){
        storage.clear();
        SymbolInfo aux;
        if(symbolNamesInfo!=null){
        for (SymbolNames.Info info: symbolNamesInfo){
            if(storage.containsKey(info.symbol)){
                symbolNamesInfo.remove(info);
            }
            else
            {
                 aux=new SymbolInfo();
                 aux.terminal=info.terminal;
                 if(!aux.terminal){
                     aux.attributes.put("inheritedGrammar","GramaticaChristiansen");
                     aux.attributes.put("synthesizedGrammar","GramaticaChristiansen");
                 }
                storage.put(info.symbol,aux);
            }
        }}
        if(symbolAttributesInfo!=null){
        for (SymbolAttributes.Info info: symbolAttributesInfo){
            if(storage.containsKey(info.symbol)){
                 aux=storage.get(info.symbol);
                 if(aux.attributes.containsKey(info.attribute)){
                     symbolAttributesInfo.remove(info);
                 }
                 else{
                     aux.attributes.put(info.attribute,info.type);
                 }
            }
        }}
        for(ChangeListener<SymbolsStorage> listener: listeners){
            listener.onChange(this);
        }
    }
    
    public void StoreAttributes(ArrayList<SymbolAttributes.Info> sai){
        symbolAttributesInfo = sai;
        StorageUpdate();
    }
    
    public void StoreNames(ArrayList<SymbolNames.Info> sni){
        symbolNamesInfo= sni;
        StorageUpdate();
    }
    
    
    /*
    public HashMap<String, ArrayList<SymbolNames.SymbolInfo>> GetStorage(){
        return storage;
    }*/
    
    
    public String ToString(){
        String ret = "";
        int attCounter;
        SymbolInfo aux;
        
        for(String key : storage.keySet()){
            attCounter=0;
            aux=storage.get(key);
            ret+="Symbol: "+ key+ " Type: ";
            if(aux.terminal){
                ret+= "Terminal\n";
            }
            else {
                ret+= "Non Terminal \n";
            }
            for(Map.Entry si : aux.attributes.entrySet()){
                ret+="\tAttribute "+ attCounter+": ClassType["+si.getValue()+"] Name["+si.getKey()+"]\n";
                attCounter++;
            }
            ret+="\n";
        }
        return ret;
    }
    
    public TextChunkDivider toStyledDocument(StyledDocument doc){
        TextChunkDivider ret = new TextChunkDivider();
        String string = "";
        int attCounter;
        int posCounter=0;
        SymbolInfo aux;
        if(doc.getLength()>0){
        try {
            doc.remove(0, doc.getLength());
        } catch (BadLocationException ex) {
            Logger.getLogger(SymbolsStorage.class.getName()).log(Level.SEVERE, null, ex);
        }}
        try {      
        for(String key : storage.keySet()){
            attCounter=0;
            aux=storage.get(key);
            string+="Symbol: "+ key+ " Type: ";
            if(aux.terminal){
                string+= "Terminal\n";
            }
            else {
                string+= "Non Terminal \n";
            }
            for(Map.Entry si : aux.attributes.entrySet()){
                string+="\tAttribute "+ attCounter+": ClassType["+si.getValue()+"] Name[";
                
                doc.insertString(posCounter, string, SimpleAttributeSet.EMPTY);
                posCounter+=string.length();
                ret.insertMark(posCounter, new MultiComentatorMark(TYPE.OPENING));
                string=si.getKey().toString();
                doc.insertString(posCounter, string, blueLetters);
                posCounter+=string.length();
                ret.insertMark(posCounter, new MultiComentatorMark(TYPE.CLOSING));
                string="]\n";
                attCounter++;
            }
            string+="\n";

        }
        doc.insertString(posCounter, string, SimpleAttributeSet.EMPTY);
        } catch (BadLocationException ex) {
                    Logger.getLogger(SymbolsStorage.class.getName()).log(Level.SEVERE, null, ex);
            try {
                doc.remove(0, doc.getLength());
            } catch (BadLocationException ex1) {
                Logger.getLogger(SymbolsStorage.class.getName()).log(Level.SEVERE, null, ex1);
            }
            ret.clear();
        }
        return ret;
        
    }
}
