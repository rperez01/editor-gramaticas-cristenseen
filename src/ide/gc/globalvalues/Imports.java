/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ide.gc.globalvalues;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */





/**Text field to get external libraries to the project
 *
 * @author Ricardo Pérez Cortés
 */
public class Imports extends GlobalValuesPopUp{


    /**
    *Type of the precedence
    */


    private static final String IMPORTS = "import";
    private static final String MENU_NAME = "Imports";
    private static ArrayList<String> info = new ArrayList();

    

    public Imports(JFrame parent){
        super(parent,MENU_NAME);
        

    }
    
    protected void NewElement(JPanel container){
        JPanel aux;
        JButton button;
        aux= new JPanel();
        button =  new JButton("X");
        button.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                container.remove(aux);
                container.updateUI();
            }
        });
        aux.add(button);
        aux.add(new JLabel(IMPORTS));
        aux.add(new JTextField(30));
        container.add(aux,container.getComponentCount() -1);
        container.updateUI();
        
    }
    

    @Override
    protected void UpdateInfo(JPanel container) {
        info.clear();
        Component components[] =container.getComponents();
        JPanel jp;
        for(int i=0;i<components.length-1;i++){
            jp =  (JPanel) components[i];
            info.add(((JTextField)jp.getComponent(2)).getText());
        }
    }

    @Override
    protected void LoadInfo(JPanel container) {
        for(String si: info){
        JPanel aux;
        JButton button;
        aux= new JPanel();
        button =  new JButton("X");
        button.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                container.remove(aux);
                container.updateUI();
            }
        });
        aux.add(button);
        aux.add(new JLabel(IMPORTS));
        aux.add(new JTextField(si,30));
            
        container.add(aux,container.getComponentCount()-1);
        
        }
        container.updateUI();
    }
                

    
    @Override
    public ArrayList<String> saveToArray(){
        return info;
    }
    
    
    @Override    
    public void loadFromArray(ArrayList array) {
       info = (ArrayList<String>) array;
       LoadInfo(super.scroll);
    }
    
    @Override
    public void Reset() {
        info.clear();
        LoadInfo(super.scroll);
    }
    
    
    
   

    
    
}


