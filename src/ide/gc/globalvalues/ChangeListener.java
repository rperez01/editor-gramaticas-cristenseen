/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ide.gc.globalvalues;

/**
 *
 * @author ricardo
 */
public interface ChangeListener<T> {
    public void onChange(T theChangedThing);
}
