/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ide.gc.globalvalues;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.AbstractAction;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

/**
 *
 * @author Ricardo Pérez Cortés
 */
public abstract class GlobalValuesPopUp extends AbstractAction{
    private static final String SAVENAME = "SAVE";
    private static final String QUITNAME = "QUIT";
    private static final String NEWELEMENT = "Add element";
    
    private JFrame parent;
    private JDialog son;
    protected JPanel scroll;
    private JButton newElement;
    private JButton save;
    private JButton quit;
    public GlobalValuesPopUp(JFrame parent ,String name){
        super(name);
        Container cp;
        JPanel jp;
        JScrollPane scroll;
        this.parent=parent;
        son = new JDialog(parent);
        cp=son.getContentPane();
        cp.setLayout(new BorderLayout());
        this.scroll=new JPanel();
        scroll= new JScrollPane(this.scroll,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        this.scroll.setLayout(new BoxLayout(this.scroll,BoxLayout.Y_AXIS));
        //this.scroll.setPreferredSize(new Dimension(Integer.MAX_VALUE,0));
            newElement = new JButton(NEWELEMENT);
            newElement.setAlignmentX(0.5f);
            newElement.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent e) {
                    NewElement(GlobalValuesPopUp.this.scroll);
                }
            });
            //newElement.setPreferredSize(new Dimension(0,0));
            
        cp.add(scroll,BorderLayout.CENTER);
        jp = new JPanel();
            save= new JButton(SAVENAME);
                save.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                UpdateInfo(GlobalValuesPopUp.this.scroll);
                son.setVisible(false);
            }
        });
            quit= new JButton(QUITNAME);
                quit.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                son.setVisible(false);
            }
        });
            jp.add(save);
            jp.add(quit);
        cp.add(jp,BorderLayout.SOUTH);
        

    }
            
    protected abstract void  NewElement(JPanel container);
    
    protected abstract void UpdateInfo(JPanel container);
    
    protected abstract void LoadInfo (JPanel container);
    
    public abstract void Reset();
    
    public abstract ArrayList saveToArray();
    
    public abstract void loadFromArray(ArrayList array);
        
    
    
    
    @Override
    public void actionPerformed(ActionEvent e) {
        this.scroll.removeAll();
        this.scroll.add(newElement);
        LoadInfo(this.scroll);
        son.setVisible(true);
       
        Dimension aux = Toolkit.getDefaultToolkit().getScreenSize();
        son.setLocation(
                aux.width/2-aux.width/8,
                aux.height/2-aux.height/8
                );
        aux.height/=4;
        aux.width/=3;
        son.setSize(aux);

       
    }
    

    
}
