/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ide.gc.globalvalues;


import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


/**
 *
 * @author Ricardo Pérez Cortés
 */
public class SymbolNames extends GlobalValuesPopUp{
    private static final String MENU_NAME = "Symbols";
    private static final String SYMBOL = "Symbol:";
    private static final String [] TERMINALLIST = {"Terminal","No Terminal"};
    private static ArrayList<Info> info = new ArrayList();
    private SymbolsStorage storage;
    

    public SymbolNames(JFrame parent){
        super(parent,MENU_NAME);
        storage = SymbolsStorage.getStorage();

    }
    
    
    @Override
    protected void NewElement(JPanel container){
        JPanel aux;
        JButton button;
        aux= new JPanel();
        button =  new JButton("X");
        button.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                container.remove(aux);
                container.updateUI();
            }
        });
        aux.add(button);
        aux.add(new JComboBox(TERMINALLIST));
        aux.add(new JLabel(SYMBOL));
        aux.add(new JTextField(5));
        container.add(aux,container.getComponentCount() -1);
        container.updateUI();
    }
    

    @Override
    protected void UpdateInfo(JPanel container) {
        info.clear();
        Component components[] =container.getComponents();
        JPanel jp;
        for(int i=0;i<components.length-1;i++){
            jp =  (JPanel) components[i];
            info.add(
            new Info(
                ((JComboBox) jp.getComponent(1)).getSelectedIndex() == 0,
                ((JTextField)jp.getComponent(3)).getText()
            ));
            
        }
        storage.StoreNames(info);
    }

    @Override
    protected void LoadInfo(JPanel container) {
        for(Info si: info){
        JPanel aux;
        JButton button;
        JComboBox jcb;
        JTextField jtf;
        aux= new JPanel();
        button =  new JButton("X");
        button.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                container.remove(aux);
                container.updateUI();
            }
        });
        aux.add(button);
        jcb=new JComboBox(TERMINALLIST);
        jcb.setSelectedIndex(si.terminal ? 0 : 1);
        aux.add(jcb);
        aux.add(new JLabel(SYMBOL));
        aux.add(new JTextField(si.symbol,5));

        container.add(aux,container.getComponentCount()-1);
        
        }
        container.updateUI();
    }

    @Override
    public void Reset() {
        info.clear();
        LoadInfo(super.scroll);
        storage.StoreNames(info);
    }

    @Override
    public ArrayList<Info> saveToArray() {
        return info;
    }

    @Override
    public void loadFromArray(ArrayList array) {
        info = (ArrayList<Info>) array;
        LoadInfo(super.scroll);
        storage.StoreNames(info);
    }
                
    
    public class Info{
        public Info(boolean terminal, String symbol){
            this.terminal=terminal;
            this.symbol=symbol;
        }
        
        public boolean terminal;
        public String symbol;
    }
    
}
