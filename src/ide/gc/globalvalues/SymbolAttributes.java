/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ide.gc.globalvalues;


import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


/**
 *
 * @author Ricardo Pérez Cortés
 */
public class SymbolAttributes extends GlobalValuesPopUp{
    private static final String MENU_NAME = "Symbol Attributes";
    private static final String TYPE = "Type:";
    private static final String SYMBOL = "Symbol:";
    private static final String ATTRIBUTE = "Attribute:";
    private static ArrayList<Info> info = new ArrayList();
    private SymbolsStorage storage;
    

    public SymbolAttributes(JFrame parent){
        super(parent,MENU_NAME);
        storage = SymbolsStorage.getStorage();
        

    }
    
    public SymbolsStorage getSymbolNamesStorage(){
        return storage;
    }
    
    @Override
    protected void NewElement(JPanel container){
        JPanel aux;
        JButton button;
        aux= new JPanel();
        button =  new JButton("X");
        button.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                container.remove(aux);
                container.updateUI();
            }
        });
        aux.add(button);
        aux.add(new JLabel(SYMBOL));
        aux.add(new JTextField(5));
        aux.add(new JLabel(TYPE));
        aux.add(new JTextField(5));
        aux.add(new JLabel(ATTRIBUTE));
        aux.add(new JTextField(5));
        container.add(aux,container.getComponentCount() -1);
        container.updateUI();
    }
    

    @Override
    protected void UpdateInfo(JPanel container) {
        info.clear();
        Component components[] =container.getComponents();
        JPanel jp;
        for(int i=0;i<components.length-1;i++){
            jp =  (JPanel) components[i];
            info.add(
            new Info(
                ((JTextField)jp.getComponent(2)).getText(),
                ((JTextField)jp.getComponent(4)).getText(),
                ((JTextField)jp.getComponent(6)).getText() 
            ));
            
        }
        storage.StoreAttributes(info);
    }

    @Override
    protected void LoadInfo(JPanel container) {
        for(Info si: info){
        JPanel aux;
        JButton button;
        aux= new JPanel();
        button =  new JButton("X");
        button.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                container.remove(aux);
                container.updateUI();
            }
        });
        aux.add(button);
        aux.add(new JLabel(SYMBOL));
        aux.add(new JTextField(si.symbol,5));
        aux.add(new JLabel(TYPE));
        aux.add(new JTextField(si.type,5));
        aux.add(new JLabel(ATTRIBUTE));
        aux.add(new JTextField(si.attribute,5));

        container.add(aux,container.getComponentCount()-1);
        
        }
        container.updateUI();
    }

    @Override
    public void Reset() {
        info.clear();
        LoadInfo(super.scroll);
        storage.StoreAttributes(info);
    }

    @Override
    public ArrayList<Info> saveToArray() {
        return info;
    }

    @Override
    public void loadFromArray(ArrayList array) {
        info = (ArrayList<Info>) array;
        LoadInfo(super.scroll);
        storage.StoreAttributes(info);
    }
                
    
    public class Info{
        public Info(String symbol, String classname,String attribute){
            this.type=classname;
            this.symbol=symbol;
            this.attribute=attribute;
        }
        
        public String type;
        public String symbol;
        public String attribute;
    }
    
}
