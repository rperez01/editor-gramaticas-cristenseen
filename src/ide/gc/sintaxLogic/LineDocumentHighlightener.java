/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ide.gc.sintaxLogic;

import TextChunkDividers.CharactersDivider;
import TextChunkDividers.EndLineMark;
import TextChunkDividers.TextChunkDivider;
import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JEditorPane;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

/**
 * Highlights the main lines when document loses focus
 * @author Ricardo Pérez Cortés
 */
public class LineDocumentHighlightener implements FocusListener{
    /*private ArrayList <LineChangedListener> listeners;
    private TextChunkDivider tcd;*/
    private JEditorPane pane;/*
    private static char  newLineChar='\n';
    private int activeLine;*/
    
    private final static Color BACKGROUNDCOLOR = new Color(0.1f, 0.5f ,0.8f);
    private final static Color FOREGROUNDCOLOR = Color.WHITE;
            
    
    private static MutableAttributeSet backgroundColoras;
    
    
    SyntaxMarker.OffsetPlusLength lastLine;
    SyntaxMarker.OffsetPlusLength lastHeader;
    
    public LineDocumentHighlightener(JEditorPane jep){
        if(backgroundColoras==null){
            backgroundColoras = new SimpleAttributeSet();
            StyleConstants.setBackground(backgroundColoras, BACKGROUNDCOLOR);
            StyleConstants.setBold(backgroundColoras, true);
            StyleConstants.setForeground(backgroundColoras, FOREGROUNDCOLOR);
        }
        
        jep.addFocusListener(this);
        
        
        pane=jep;
        
        //doc=jep.getDocument();
        
       /* listeners = new ArrayList();
        tcd= new TextChunkDivider();
        d.addDocumentListener(this);

        doc=d;
        activeLine=0;*/
    }
    
    public void changeLineBackground(int position){
        StyledDocument doc = ((StyledDocument)pane.getDocument() ); 
        SyntaxMarker marker = SyntaxMarker.getInstance();
        lastHeader = marker.getHeader(position);
        doc.setCharacterAttributes(lastHeader.offset, lastHeader.length, backgroundColoras, true);
        if(marker.isHeader(position)){
            lastLine = null;
            
        }
        else{
           lastLine = marker.getLine(position);
           doc.setCharacterAttributes(lastLine.offset, lastLine.length, backgroundColoras, true);
        }
    }
    
    public void resetBackground(){
        StyledDocument doc = ((StyledDocument)pane.getDocument() ); 
        if(lastLine!=null)
            doc.setCharacterAttributes(lastLine.offset, lastLine.length, SimpleAttributeSet.EMPTY, true);
        if(lastHeader!=null)
            doc.setCharacterAttributes(lastHeader.offset, lastHeader.length, SimpleAttributeSet.EMPTY, true);
        
    }

    @Override
    public void focusGained(FocusEvent e) {
        resetBackground();
    }

    @Override
    public void focusLost(FocusEvent e) {
        Caret ca =pane.getCaret();
        changeLineBackground(ca.getDot());
    }
}

    /*
    private int checkNewLines(String s){
        for(int i=0; i<s.length();i++){
            if(s.charAt(i)==newLineChar){
                return i;
            }
        }
        return -1;
    }
    
    public boolean addLineChangedListener(LineChangedListener listener){
        return listeners.add(listener);
    }
    
    public boolean removeLineChangedListener(LineChangedListener listener){
        return listeners.remove(listener);
    }
    
    public getActiveLine(){
        
    }
    
    @Override
    public void caretUpdate(CaretEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        String changedString;
        int newLinePos,index;
        tcd.insertUpdate(e);
        try {
             changedString =doc.getText(e.getOffset(), e.getLength());
             newLinePos=checkNewLines(changedString);
             index=tcd.getNearestMarkPos(e.getOffset());
             if(newLinePos!=-1){
                 
                 String [] changedLines= changedString.split( Character.toString(newLineChar) );
                 for(int i=1;i<changedLines.length;i++){
                    tcd.insertMark(newLinePos+e.getOffset(), new EndLineMark());
                    newLinePos+=changedLines[i].length()+1;
                    for (LineChangedListener l : listeners){
                        l.addedLine(i+index);
                    }
                 }
             }
             String change= doc.getText(tcd.getNearestMarkPos(e.getOffset()), tcd.getNextMarkPosition(e.getOffset()));
             for (LineChangedListener l : listeners){
                         l.changedLine(index, change);
                }
             
             
             
        } catch (BadLocationException ex) {
            Logger.getLogger(LineDocumentHighlightener.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        int index1, index2;
        index1=tcd.getNearestMarkPos(e.getLength()+e.getOffset());
        tcd.removeUpdate(e);
        index2=tcd.getNearestMarkPos(e.getLength()+e.getOffset());
        while (index2<index1){
            index2++;
            for(LineChangedListener l: listeners){
                l.removedLine(index2);
            }
        }
        String change;
        try {
            change = doc.getText(tcd.getNearestMarkPos(e.getOffset()), tcd.getNextMarkPosition(e.getOffset()));
            for (LineChangedListener l : listeners){
                         l.changedLine(index1, change);
                }
        } catch (BadLocationException ex) {
            Logger.getLogger(LineDocumentHighlightener.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public void changedUpdate(DocumentEvent e) {
    }*/
