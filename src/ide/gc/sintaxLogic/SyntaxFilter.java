/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ide.gc.sintaxLogic;

import TextChunkDividers.TextChunkDivider;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;
import javax.swing.text.SimpleAttributeSet;

/**
 * Ensures the correct function of the syntax
 * @author Ricardo Pérez Cortés
 */
public class SyntaxFilter extends DocumentFilter{
    //private static final AttributeSet DEFAULTATTRIBUTESET = new AttributeSet();
    private int newOffset;
    @Override
    public void remove(FilterBypass fb, int offset, int length) throws
                       BadLocationException {
        Document doc;
        int beginLine,endLine,cQuotationInit=-1,cQuotationEnd;
        int preRemove=0, postRemove=0;
        char [] charArray;
        char lastCharInit, bLastCharInit;
        char lastCharEnd, bLastCharEnd;
        char firstCharEnd, secondCharEnd,thirdCharEnd;
        
        
        TextChunkDivider nl;
        String preRemoval, postRemoval, removal;
        doc=fb.getDocument();
        nl= SyntaxMarker.getInstance().getNewLineMarker();
        beginLine=nl.getPreviousMarkPosition(offset-1);
        endLine=nl.getNextMarkPosition(offset+length-1);
        if(endLine==-1)
            endLine=doc.getLength();
        preRemoval=doc.getText(beginLine, offset-beginLine);
        removal=doc.getText(offset, length);
        postRemoval=doc.getText(offset+length, endLine-offset-length);
        charArray=preRemoval.toCharArray();
        bLastCharInit=lastCharInit='\0';
        for(char c : charArray){
            if(c=='\''){
                if(cQuotationInit<0&&lastCharInit!='\\'){
                    cQuotationInit=2;
                }
            }
            bLastCharInit=lastCharInit;
            lastCharInit=c;
            if(lastCharInit!='\\')
                cQuotationInit--;
            else
                lastCharInit='*';


        }
        cQuotationEnd=cQuotationInit;
        lastCharEnd=lastCharInit;
        bLastCharEnd=bLastCharInit;
        charArray=removal.toCharArray();
        for(char c : charArray){
            if(c=='\''){
                if(cQuotationEnd<0&&lastCharEnd!='\\'){
                    cQuotationEnd=2;
                }
            }
            bLastCharEnd=lastCharEnd;
            lastCharEnd=c;
            if(lastCharEnd!='\\')
                cQuotationEnd--;
            else
                lastCharEnd='*';
            if(c==':'&&cQuotationEnd<0){
                if(SyntaxMarker.getInstance().getNewLineMarker().getPreviousMarkPosition(offset-preRemove)==0)
                return;
            }

        }
        firstCharEnd=secondCharEnd=thirdCharEnd='\0';
        charArray=postRemoval.toCharArray();
        if(charArray.length>=1){
            firstCharEnd=charArray[0];
            if(charArray.length>=2)
            {
                secondCharEnd=charArray[1];
                if(charArray.length>=3)
                    thirdCharEnd=charArray[2];
            }
        }

        if(lastCharInit==':'&&cQuotationInit<0){
            
            preRemove=1;
            if(SyntaxMarker.getInstance().getNewLineMarker().getPreviousMarkPosition(offset-preRemove)==0)
                return;
        }
        if(cQuotationInit>=0){
            if(preRemoval.charAt(preRemoval.length()+cQuotationInit-2)=='\'')
                preRemove=2-cQuotationInit;
            else
                preRemove=3-cQuotationInit;
        }
        if(cQuotationEnd>=0){
            if(firstCharEnd=='\'')
                postRemove=1;
            else if (thirdCharEnd=='\'' && firstCharEnd=='\\')
                postRemove=3;
            else if (secondCharEnd=='\'' && cQuotationEnd!=0){
                postRemove=2;
            }
        }
        boolean notStable=true;
        boolean needSpace=false;
        char preChar,postChar;
        if(preRemoval.length()-preRemove<=0)
            preChar='\0';
        else
            preChar=preRemoval.charAt(preRemoval.length()-preRemove-1);
        if(postRemoval.length()-postRemove<=0){
            postChar='\0';
        }
        else
        {
            postChar=postRemoval.charAt(postRemove);
        }
        if((preChar=='\''&&postChar!=' '&&postChar!='\0'&&postChar!='\n'&&postChar!=':')||
                (postChar=='\''&&preChar!=' '&&preChar!='\0'&&preChar!='\n')){
            preChar=' ';
            needSpace=true;
        }
        if(!Character.isLetterOrDigit(preChar)&&Character.isDigit(postChar)){
            postRemove=numberWordInconsistence(postRemoval,postRemove);
            needSpace=false;
        }
        newOffset=offset-preRemove;
        if(needSpace)
            fb.replace(newOffset, length+preRemove+postRemove, " ", SimpleAttributeSet.EMPTY);
        else
            fb.remove(newOffset, length+preRemove+postRemove);
    }
    
    /**/
    private int numberWordInconsistence(String s,int offset){
            int i=offset;
            while(i<s.length()){
                char aux=s.charAt(i);
                if(aux==' '||aux=='\n'||aux==':'){
                    break;
                }
                i++;
            }
            return i;
    }
            
            
    @Override
    public void insertString(FilterBypass fb, int offset, String string,
                             AttributeSet attr) throws BadLocationException {
        int beginLine,endLine,lastLength;
        char [] charArray;
        char lastChar, bLastChar;
        char firstCharEnd,secondCharEnd,thirdCharEnd;
        int postRemove=0;
        int cQuotation = -1,cQuotationEnd,removeOffset=0;
        boolean alreadyRule=false;
        Document doc;
        TextChunkDivider nl;
        String preInsertion, postInsertion, parsedString;
        doc=fb.getDocument();
        nl= SyntaxMarker.getInstance().getNewLineMarker();
        beginLine=nl.getPreviousMarkPosition(offset-1);
        endLine=nl.getNextMarkPosition(offset-1);
        if(endLine==-1)
            endLine=doc.getLength();
        preInsertion=doc.getText(beginLine, offset-beginLine);
        postInsertion=doc.getText(offset, endLine-offset);
        lastChar=bLastChar='\0';
        charArray=preInsertion.toCharArray();
        for(char c : charArray){
            if(c=='\''){
                if(cQuotation<0&&lastChar!='\\'){
                    cQuotation=2;
                }
            }
            bLastChar=lastChar;
            lastChar=c;
            if(lastChar!='\\'||bLastChar!='\'')
                cQuotation--;
            else
                lastChar='*';
            
        }
        cQuotationEnd=cQuotation;
        charArray=string.toCharArray();
        parsedString="";
        lastLength=0;
        
        
        
        
        
        
        for (char c : charArray){
            if(cQuotation==0){
                parsedString+='\'';
                if(c!='\''){
                    bLastChar=lastChar;
                    lastChar='\'';
                    cQuotation--;
                }
                
            }
            
            if (c=='\n'){
                if(alreadyRule||SyntaxMarker.getInstance().getNewLineMarker().getPreviousMarkPosition(offset)!=0){
                    parsedString+=c;
                    lastChar='\0';
                    cQuotation=-1;
                }
            }
            else if(lastChar ==':'&& (cQuotation<0))
            {
                continue;
            }
            else if(c=='\''){
                if(cQuotation<0){
                    if(lastChar==' '||lastChar=='\n'||lastChar=='\0')
                    {
                        parsedString+=c;
                        cQuotation=2;
                    }
                }
                
            }
            else if(c == ':'&&lastChar!=':'){
                if(cQuotation<0)
                    alreadyRule=true;
                parsedString+=":";
            }
            else if ( Character.isLetter(c)){
                if(lastChar!='\''||cQuotation>=0)
                    parsedString+=c;
            }
            else if ( Character.isDigit(c)){
                if(lastChar!=' '&&lastChar!='\n'&&lastChar!='\0')
                    parsedString+=c;
            }
            else if (c==' '){
                parsedString+=c;
            }
            else{
                if(cQuotation>=0){
                    parsedString+=c;
                }
            }
            
            if(lastLength!=parsedString.length()){
                lastLength=parsedString.length();
                if(lastChar!='\\'||bLastChar!='\''){
                    bLastChar=lastChar;
                    lastChar=parsedString.charAt(lastLength-1);
                    cQuotation--;
                }
                else{
                    lastChar='*';
                }
            }
            
            
        }
        if(lastChar==':'&&!parsedString.isEmpty()&&cQuotation<0){
                parsedString+='\n';
                bLastChar='\0';
                lastChar='\n';
        }
        firstCharEnd=secondCharEnd=thirdCharEnd='\0';
        charArray=postInsertion.toCharArray();
        if(charArray.length>=1){
            firstCharEnd=charArray[0];
            if(charArray.length>=2)
            {
                secondCharEnd=charArray[1];
                if(charArray.length>=3)
                    thirdCharEnd=charArray[2];
            }
        }
        if(cQuotationEnd>=0){
            if(firstCharEnd=='\'')
                postRemove=1;
            else if (thirdCharEnd=='\'' && firstCharEnd=='\\')
                postRemove=3;
            else if (secondCharEnd=='\'' && cQuotationEnd!=0){
                postRemove=2;
            }
        }
        
        if(cQuotation>=0){
            if(cQuotation==0){
                if(lastChar=='\\'&&bLastChar=='\'')
                    parsedString+=" \'";
                else
                    parsedString+="\'";
            }
            else parsedString+=" \'";
        }
        
        char preChar, postChar;
        boolean needSpace=false;
        
        if(parsedString.length()<=0){
            if(preInsertion.length()<=0)
                preChar='\0';
            else
                preChar=preInsertion.charAt(preInsertion.length()-1);
        }
        else
            preChar=parsedString.charAt(parsedString.length()-1);
        if(postInsertion.length()-postRemove<=0){
            postChar='\0';
        }
        else
        {
            postChar=postInsertion.charAt(postRemove);
        }
        if((preChar=='\''&&postChar!=' '&&postChar!='\0'&&postChar!='\n'&&postChar!=':')||
                (postChar=='\''&&preChar!=' '&&preChar!='\0'&&preChar!='\n')){
            preChar=' ';
            needSpace=true;
        }
        if(!Character.isLetterOrDigit(preChar)&&Character.isDigit(postChar)){
            postRemove=numberWordInconsistence(postInsertion,postRemove);
            needSpace=false;
        }
        if(needSpace)
            parsedString+=" ";
        
        fb.replace(offset, postRemove, parsedString, SimpleAttributeSet.EMPTY);
    }
    /**
     *
     */
    @Override
    public void replace(FilterBypass fb, int offset, int length, String text,
                        AttributeSet attrs) throws BadLocationException {
        if(length>0){
            remove(fb,offset,length);
            insertString(fb,newOffset,text,null);
        }
        else
            insertString(fb,offset,text,null);
        
        
    }
    
}
