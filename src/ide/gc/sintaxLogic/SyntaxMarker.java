/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ide.gc.sintaxLogic;

import InternalLogic.Project;
import TextChunkDividers.CharactersDivider;
import TextChunkDividers.EmptyMark;
import TextChunkDividers.TextChunkDivider;
import javax.swing.text.AbstractDocument;
import javax.swing.text.Document;

/**
 *Singleton that agroups the marks of the syntatic zone
 * @author Ricardo Pérez Cortés
 */
public class SyntaxMarker {
    private static SyntaxMarker instance=null;
    private static final char[] NEWLINE= {'\n'};
    private static final char[] NEWRULE= {':','\n'};
    private final Project proj;
    private final AbstractDocument doc;
    private final CharactersDivider dividerNewLine;
    private final CharactersDivider dividerNewRule;
    public static void createInstance(AbstractDocument doc){
        if(instance ==null){
            instance = new SyntaxMarker(doc);
        }
    }
    
    public static SyntaxMarker getInstance(){
        return instance;
    }
    
    private SyntaxMarker(AbstractDocument doc){
        this.doc=doc;
        proj= new Project (doc);
        dividerNewLine=  new CharactersDivider(doc,NEWLINE);
        dividerNewRule= new CharactersDivider(doc,NEWRULE);
        proj.setStuff(dividerNewLine.tcd,dividerNewRule.tcd,this);
       
    }
    
    public TextChunkDivider getNewLineMarker(){
        return dividerNewLine.tcd;
    }
    
    public TextChunkDivider getSyntaxMarker(){
        return dividerNewRule.tcd;
    }
    
    public boolean isHeader(int position){
        TextChunkDivider nl, nr;
        int nextRule,nextLine;
        nl = dividerNewLine.tcd;
        nr = dividerNewRule.tcd;
        nextRule=nr.getNextMarkPosition(position-2);
        if(nextRule==-1){
            return nr.getPreviousMark(position-1) instanceof EmptyMark;
        }
        else{
            nextLine=nl.getNextMarkPosition(position-1);
            if(nextLine==-1)
                return true;
            else
                return (nextLine>nextRule);
        }
        
    }
    
    /**
     * Gets the offset and the length of the nearest header
     * @param position the position where to find the nearest header
     * @return the offset and the length of the header
     */
    public OffsetPlusLength getHeader(int position){
        TextChunkDivider nl,nr;
        OffsetPlusLength ret = new OffsetPlusLength();
        int positionAux;
        nl = dividerNewLine.tcd;
        nr = dividerNewRule.tcd;
        if(isHeader(position)){
            ret.offset=nl.getPreviousMarkPosition(position);
            ret.length=nl.getNextMarkPosition(position);
        }
        else{
            positionAux=nr.getPreviousMarkPosition(position);
            ret.offset=nl.getPreviousMarkPosition(positionAux);
            ret.length=nl.getNextMarkPosition(positionAux);
        }
        if(ret.length<0){
            ret.length=doc.getLength();
        }
        ret.length-=ret.offset;
        return ret;
    }
    /**
     * Gets the offset and the length of the line where it is position
     * @param position the position where to find the lline
     * @return the offset and the length of the line
     */
    public OffsetPlusLength getLine(int position){
        TextChunkDivider nl;
        OffsetPlusLength ret = new OffsetPlusLength();
        nl = dividerNewLine.tcd;
        ret.offset=nl.getPreviousMarkPosition(position-1);
        ret.length=nl.getNextMarkPosition(position-1);
        if(ret.length<0){
            ret.length=doc.getLength();
        }
        ret.length-=ret.offset;
        return ret;
    }
    
    public class OffsetPlusLength {
        public int offset;
        public int length;
    }
    
    public Project getProject(){
        return proj;
    }
}
