/*

* To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ide.gc.sintaxLogic;

/**
 * Listener warned when the number of lines changes in a document
 * @author Ricardo Pérez Cortés
 */
public interface LineChangedListener {
    
    /**
     * Warns when some line has been removed
     * @param n The number of the line removed
     */
    public void removedLine (int n);
    /**
     * Warns when some line has been added
     * @param n The number of the line added
     */
    public void addedLine(int n);
    
    /**
     * Warns when the text of a line has changed
     * @param n the line whose text changed
     * @param text the new text of the line
     */
    public void changedLine(int n, String text);
}
