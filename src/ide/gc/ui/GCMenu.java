/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ide.gc.ui;

import Compiler.CompileMain;
import InternalLogic.SaveLoader;
import java.awt.event.KeyEvent;
import ide.gc.IDEGC;
import ide.gc.actions.MyUndoableEditListener;
import ide.gc.actions.RedoAction;
import ide.gc.actions.UndoAction;
import ide.gc.globalvalues.Imports;
import ide.gc.globalvalues.Precedence;
import ide.gc.globalvalues.SymbolAttributes;
import ide.gc.globalvalues.SymbolNames;
import ide.gc.sintaxLogic.SyntaxMarker;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeListener;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.text.DefaultEditorKit.*;
import javax.swing.undo.UndoManager;

/**
 *  Class that adds the mnu bar to the application
 * @author Ricardo Pérez Cortés
 */
public class GCMenu extends JMenuBar{
    private SymbolNames sn;
    private Precedence p;
    private SymbolAttributes sa;
    private Imports imp;
    public GCMenu(JFrame parent) {
        super();
        JMenu m;
        JMenuItem item;
        Action action;
        RedoAction ra;

        UndoAction ua;
        UndoManager um;
        SaveLoader sl = new SaveLoader();
            m=new JMenu("File");
                //m.setMnemonic(KeyEvent.VK_F);
                item = new JMenuItem("New");
                item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N,ActionEvent.CTRL_MASK, true));
                item.addActionListener(sl);
                m.add(item);
                item = new JMenuItem("Open");
                item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O,ActionEvent.CTRL_MASK|ActionEvent.SHIFT_MASK,true));
                item.addActionListener(sl);
                m.add(item);
                item = new JMenuItem("Save");
                item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,ActionEvent.CTRL_MASK,true));
                item.addActionListener(sl);
                m.add(item);
                item = new JCheckBoxMenuItem("Useless option");
                m.add(item);
                item = new JMenuItem("Exit");
                item.addActionListener(new ActionListener(){
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        IDEGC.mWindow.dispose();
                    }
                    
                });
                m.add(item);
                /*item = new JMenuItem("TestProject");
                item.addActionListener(new ActionListener(){
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        System.out.print("Proyecto:\n");
                        System.out.print(SyntaxMarker.getInstance().getProject().toString());
                    }

                });
                m.add(item);*/
                
            this.add(m);
            m=new JMenu("Edit");
                action = new CutAction();
                action.putValue(Action.NAME, "Cut");
                m.add(action);
                action = new CopyAction();
                action.putValue(Action.NAME, "Copy");
                m.add(action);
                action = new PasteAction();
                action.putValue(Action.NAME, "Paste");
                m.add(action);
                m.addSeparator();
                ua = new UndoAction();
                m.add(ua);
                ra = new RedoAction();
                ra.linkUndo(ua);
                m.add(ra);
                MyUndoableEditListener.getInstance().set(ua).set(ra);
            this.add(m);
            m=new JMenu("Global Values");
                sn=new SymbolNames(parent);
                sa=new SymbolAttributes(parent);
                p=new Precedence(parent);
                imp = new Imports (parent);
                m.add(imp);
                m.add(sn);
                m.add(sa);
                m.add(p);
                
            this.add(m);
            
            this.add(new JButton(new CompileMain()));
            
    }
    
    public Precedence getPrecedence(){
        return p;
    }
    
    public Imports getImports(){
        return imp;
    }
    
    public SymbolNames getSymbolNames(){
        return sn;
    }

    public SymbolAttributes getSymbolAttributes() {
        return sa;
    }
}
