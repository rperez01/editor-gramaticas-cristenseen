/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ide.gc.ui;

import InternalLogic.SemanticLogic;
import ide.gc.actions.MyUndoableEditListener;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.undo.UndoManager;

/**
 *
 * @author usuario
 */
public class MainWindow extends JFrame{
    public static final Dimension ZERO_DIMENSION = new Dimension(0,0);
    public static final Border TEXT_BORDER =  BorderFactory.createCompoundBorder(
            BorderFactory.createEmptyBorder(0,0,0,3),
            BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
    private static MainWindow instance;
    Container cp;
    JPSyntax sintaxis;
    JPAuxiliarDisplay tree;
    JPSemantica semantica;
    GCMenu menu;
    
    private MainWindow(String title){
        super(title);
        instance=this;
        SemanticLogic sl;
        Border separator;
        GridBagConstraints c;
        this.setSize(Toolkit.getDefaultToolkit().getScreenSize());//getScreenSize());
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        cp=getContentPane();
        menu = new GCMenu(this);
        this.setJMenuBar(menu);
        cp.setLayout(new GridBagLayout());
        c = new GridBagConstraints();
        separator = BorderFactory.createMatteBorder(0,0,0,2,Color.lightGray);
        c.fill=GridBagConstraints.BOTH;
        c.anchor=GridBagConstraints.CENTER;
        c.weightx=2;
        c.weighty=3;
        c.gridx=0;
        c.gridy=0;
        sintaxis=new JPSyntax ();
        sintaxis.setBorder(separator);
        cp.add(sintaxis,c);
        c.weightx=1;
        c.gridx=1;
        c.gridy=0;
        tree=new JPAuxiliarDisplay();
        cp.add(tree,c);
        
        c.weighty=2;
        c.gridx=0;
        c.gridy=1;
        c.gridwidth=2;
        c.fill=GridBagConstraints.BOTH;
        semantica= new JPSemantica();
        separator = BorderFactory.createMatteBorder(2,0,0,0,Color.lightGray);
        semantica.setBorder(separator);
        cp.add(semantica,c);
        
        
        
        
        this.setVisible(true);
        
        sl = new SemanticLogic(semantica.getSelector(),sintaxis.getJPane());
        instance=this;
    }
    
    public static MainWindow getWindow(){
        return instance;
    }
    
    public static MainWindow CreateWindow(String tittle){
        if (instance==null)
            new MainWindow(tittle);
        return instance;
    }
    
    public GCMenu getGCMenu() {
        return menu;
    }
    
    public JPSyntax getJPSyntax(){
        return sintaxis;
    }
    
    public JPAuxiliarDisplay getJPAuxiliarDisplay(){
        return tree;
    }
    
}
