/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ide.gc.ui;

import InternalLogic.JavaCode;
import ide.gc.actions.MyUndoableEditListener;
import ide.gc.actions.UndoSettler;
import ide.gc.actions.wordAnalizing.JavaDocumentHighlightener;
import ide.gc.actions.wordAnalizing.JavaWordHighlightener;
import java.awt.Color;
import java.awt.FlowLayout;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import javax.swing.text.StyledDocument;

/**
 *
 * @author ricardo
 */
public class JPSemanticHeader extends JScrollPane{
    JPLineCounter lineCounter;
    AbstractDocument doc;
    JavaWordHighlightener jwhl;
    JavaDocumentHighlightener jdhl;
    JTextPane text;
    UndoSettler us;
    public JPSemanticHeader(){
            super();

            /*JPanel writingZone = new JPanel();
                writingZone.setLayout(new BorderLayout());*/
            text=new JTextPane();
            us=new UndoSettler();
            text.addFocusListener(us);
            doc=(AbstractDocument) text.getDocument();
            try {
                jwhl = new JavaWordHighlightener();
                jwhl.setStyledDocument((StyledDocument) doc);
                jdhl=new JavaDocumentHighlightener(doc);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(JPSemantica.class.getName()).log(Level.SEVERE, null, ex);
            }
            doc.addUndoableEditListener(MyUndoableEditListener.getInstance());
            
            lineCounter=new JPLineCounter(text);
        //panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        this.setPreferredSize(MainWindow.ZERO_DIMENSION);
        this.setRowHeaderView(lineCounter);
        this.setViewportView(text);
        this.setViewportBorder(MainWindow.TEXT_BORDER);
        this.updateUI();
    }
        
    public void setText(String text){
        us.focusGained(null);
        this.jdhl.set(text);
        
    }

    
    public String getText() {
        try {
            return doc.getText(0, doc.getLength());
        } catch (BadLocationException ex) {
            Logger.getLogger(JPSemantica.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}
