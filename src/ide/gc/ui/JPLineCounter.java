/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ide.gc.ui;

import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.font.TextAttribute;
import java.text.AttributedCharacterIterator;
import java.text.Bidi;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.JTextComponent;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

/**
 *
 * @author usuario
 */
public class JPLineCounter extends JPanel implements DocumentListener, CaretListener{
    private static final Color LC_COLOR = new Color(185,185,190);
    private int nLines;
    private int oldnLines;
    private StyledDocument doc;
    private StyledDocument doc2;
    private JTextPane ta;
    private boolean lastLineEnter;
    private int lastLineOffset;
   public JPLineCounter (JTextPane pane){
       super();
       pane.getDocument().addDocumentListener(this);
       //pane.setEditable(false);
       nLines=1;
       oldnLines=1;
        ta = new JTextPane();
        ta.setEditable(false);
        ta.setText("1");
        pane.addCaretListener(this);
        ta.setMargin(new Insets(1,1,1,1));
        doc= (StyledDocument) ta.getDocument();
        doc2=pane.getStyledDocument();

        ta.selectAll();
        MutableAttributeSet set = new SimpleAttributeSet(pane.getParagraphAttributes());
        StyleConstants.setLineSpacing(set, 0.2f);
        doc.putProperty(TextAttribute.RUN_DIRECTION,TextAttribute.RUN_DIRECTION_RTL);
        doc.setParagraphAttributes(0,doc.getLength(),set, false);
        //doc.putProperty(TextAttribute., pane);
        //doc.putProperty(TextAttribute.BIDI_EMBEDDING,-1);
        //ta.setEditable(false);
        ta.setOpaque(false);
        ta.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
//ta.setCharacterAttributes(pane.getCharacterAttributes(), true);
        
       this.add(ta);
       //this.setBorder(BorderFactory.createLineBorder(Color.gray));
       this.setBackground(LC_COLOR);

       this.updateUI();
       
   }
   
   
   private void updateText(){
        String text;

       if (nLines>oldnLines){
           text="";
            for(oldnLines++;nLines>oldnLines;oldnLines++){
                text+="\n"+oldnLines;
            }
            text+="\n"+oldnLines;
            int maxOffset=doc.getLength();
            try {
                doc.insertString(maxOffset, text, null);
            } catch (BadLocationException ex) {
                Logger.getLogger(JPLineCounter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
       else if(nLines<oldnLines)
       {
           text="1";
            for(oldnLines=2;nLines>=oldnLines;oldnLines++){
                text+="\n"+oldnLines;
            }
            oldnLines=nLines;
            //text+="\n"+oldnLines;
            ta.setText(text);
        }
       
   }
   
   public void add(){
       nLines++;
       if(nLines<=0){
           nLines=0;
       }
       this.updateText();
   }
   
   public void remove(){
       nLines--;
       if(nLines<=0){
           nLines=1;
       }
       this.updateText();
   }
   
   public void set(int nlines){
       nLines=nlines;
       if(nLines<=0){
           nLines=1;
       }
       this.updateText();       
   }

    @Override
    public void insertUpdate(DocumentEvent e) {
        char[] newText;
        int newLines=0;
        boolean lastnoR=true;
        try {
            newText=e.getDocument().getText(e.getOffset(), e.getLength()).toCharArray();
            for(int i=0;i<newText.length;i++){
                if(newText[i]=='\n'&&lastnoR){
                    newLines++;
                    lastnoR=true;
                }
                else if(newText[i]=='\r' ){
                    newLines++;
                    lastnoR=false;
                }
                else{
                    lastnoR=true;
                }
            }
            if(newLines!=0)
                this.set(newLines+nLines);
        } catch (BadLocationException ex) {
        }
    }

    @Override
    public void removeUpdate(DocumentEvent e) { 
        if(lastLineOffset==e.getOffset()+1&&e.getLength()==1){
            if(lastLineEnter)
                remove();
        }
        else{
            char[] newText;
            int newLines=0;
            boolean lastnoR=true;
            try {
                newText=doc2.getText(0, doc2.getLength()).toCharArray();
                for(int i=0;i<newText.length;i++){
                    if(newText[i]=='\n'&&lastnoR){
                        newLines++;
                        lastnoR=true;
                    }
                    else if(newText[i]=='\r' ){
                        newLines++;
                        lastnoR=false;
                    }
                    else{
                        lastnoR=true;
                    }
                }
                this.set(newLines+1);
            } catch (BadLocationException ex) {
            }
        }
        
    }

    @Override
    public void changedUpdate(DocumentEvent e) {}

    @Override
    public void caretUpdate(CaretEvent e) {
        char lastElement;
        lastLineOffset=e.getDot();
        //System.out.println(lastLineOffset);
        try {
            String aux= doc2.getText(lastLineOffset-1,1);
            
            lastElement=aux.charAt(0);
            //System.out.println("Aux :"+aux);

        } catch (BadLocationException ex) {
            lastElement='0';
            //System.out.println("error");
        }
        lastLineEnter=false;
        lastLineEnter|=lastElement=='\n';
        lastLineEnter|=lastElement=='\r';
        //System.out.println("hola");
        
    }
}