/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ide.gc.ui;

import java.util.ArrayList;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * UI Code for consols to output information about compilation errors
 * @author Ricardo Pérez Cortés
 */
public class JPConsole extends JScrollPane{
    private final static ArrayList<JPConsole> consoles =new ArrayList<>();
    private JTextArea text;
    public JPConsole(){
        super(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        text=new JTextArea();
        this.setViewportView(text);
        text.setEditable(false);
        consoles.add(this);
    }
    
    public static void addText(String text){
        for(JPConsole console : consoles){
            console.text.append(text+"\n");
        }
    }
    
    public static void clear(){
        for(JPConsole console : consoles){
            console.text.setText("");
        }
    }
}
