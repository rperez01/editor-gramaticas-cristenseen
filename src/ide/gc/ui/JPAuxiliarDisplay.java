/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ide.gc.ui;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

/**
 * Left panel used to store auxiliary info
 * @author usuario
 */
public class JPAuxiliarDisplay extends JTabbedPane{
    private JPSemanticHeader semHead;
     public JPAuxiliarDisplay(){
         super();
         this.addTab("Symbol Info", new JPSymbols());
         semHead=new JPSemanticHeader();
         this.addTab("Semantic Header", semHead);
         this.addTab("Console", new JPConsole());
     }
    
     public JPSemanticHeader getSemanticHeader(){
         return semHead;
     }
}
