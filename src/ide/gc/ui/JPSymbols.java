/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ide.gc.ui;

import InternalLogic.JavaCode;
import TextChunkDividers.TextChunkDivider;
import ide.gc.globalvalues.ChangeListener;
import ide.gc.globalvalues.SymbolsStorage;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;

/**
 * Tab of the auxiliar panel that shows information about the declared symbols
 * @author ricardo
 */
public class JPSymbols extends JScrollPane implements ChangeListener<SymbolsStorage>, CaretListener{

    private final JTextPane text;
    private TextChunkDivider tcd;
    private  JPSemantica semantica;
    public JPSymbols() {
        super(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
            text=new JTextPane();
            text.setEditable(false);
        this.setViewportView(text);
        SymbolsStorage storage= SymbolsStorage.getStorage();
        storage.addListener(this);
        text.addCaretListener(this);
        tcd= new TextChunkDivider();
    }

    @Override
    public void onChange(SymbolsStorage theChangedThing) {
        text.removeCaretListener(this);
        tcd=theChangedThing.toStyledDocument(text.getStyledDocument());
        text.addCaretListener(this);
    }

    @Override
    public void caretUpdate(CaretEvent ce) {
        int aux;
        if(tcd.isInside(ce.getDot())&&!JavaCode.isUnactive()){
            try {
                if(semantica==null)
                    semantica=MainWindow.getWindow().semantica;
                semantica.doc.insertString(semantica.text.getCaretPosition(),
                        text.getDocument().getText(aux=tcd.getPreviousMarkPosition(ce.getDot()), tcd.getNextMarkPosition(ce.getDot())-aux),
                        SimpleAttributeSet.EMPTY);
            } catch (BadLocationException ex) {
                Logger.getLogger(JPSymbols.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
