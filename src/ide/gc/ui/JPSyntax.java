/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ide.gc.ui;


import ide.gc.actions.MyUndoableEditListener;
import ide.gc.actions.UndoSettler;
import ide.gc.sintaxLogic.LineDocumentHighlightener;
import ide.gc.sintaxLogic.SyntaxFilter;
import ide.gc.sintaxLogic.SyntaxMarker;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.AbstractDocument;
import javax.swing.text.DocumentFilter;
import javax.swing.text.StyledDocument;
/**
 *
 * @author usuario
 */
public class JPSyntax extends JScrollPane {
    private JTextPane text;
    //private JPLineCounter counter;
    private AbstractDocument doc;
    private DocumentFilter df;
    public JPSyntax (){
        super();
        text=new JTextPane();
        //text.setContentType("text/html");
        text.addFocusListener(new UndoSettler());
        doc=(AbstractDocument) text.getDocument();
        //doc.addUndoableEditListener(MyUndoableEditListener.getInstance());
        /*text.setText(" <html>\n" +
"   <head>\n" +
"     <title>Syntax</title>\n" +
"     <style type=\"text/css\">\n" +
"     </style>\n" +
"   </head>\n" +
"   <body>\n" +
"<div>\n"+
"</div>\n"+
"   </body>\n" +
" </html>\n");*/
        this.setViewportView(text);
        this.setPreferredSize(MainWindow.ZERO_DIMENSION);
        this.setViewportBorder(MainWindow.TEXT_BORDER);
        //counter = new JPLineCounter(text);
        //this.setRowHeaderView(counter);
        SyntaxMarker.createInstance(doc);
        df= new SyntaxFilter();
        doc.setDocumentFilter(df);
        new LineDocumentHighlightener(text);
        this.updateUI();
    }
    
    public JTextPane getJPane(){
        return text;
    }

}
