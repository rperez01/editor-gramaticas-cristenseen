/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ide.gc.actions;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.undo.UndoManager;

/**
 *
 * @author usuario
 */
public class UndoSettler implements FocusListener{
     UndoManager um;
    
    public UndoSettler(){
        um = new UndoManager();
    }

    @Override
    public void focusGained(FocusEvent e) {
        MyUndoableEditListener.getInstance(um);
    }

    @Override
    public void focusLost(FocusEvent e) {
      //  MyUndoableEditListener.getInstance(null);
    }
}
