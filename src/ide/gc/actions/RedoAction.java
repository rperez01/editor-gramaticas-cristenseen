/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ide.gc.actions;

import java.awt.event.KeyEvent;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import static javax.swing.Action.ACCELERATOR_KEY;
import javax.swing.KeyStroke;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.UndoManager;

/**
 * Intenta rehacer una acción
 * @author Ricardo Pérez Cortés
 */
public class RedoAction extends AbstractAction{
    protected  UndoManager undoManager;
    protected UndoAction undo;
    
    public void setUndoManager(UndoManager undoManager){
        this.undoManager=undoManager;
        if(undoManager!=null){
            this.updateStatus();
        }
        else{
            setEnabled(false);
            putValue(Action.NAME,"Redo");
        }
    }
    
    public RedoAction(){
        super();
        if(undoManager==null){
            putValue(Action.NAME,"Redo");
            setEnabled(false);
        }
        else
            updateStatus();
        this.putValue(ACCELERATOR_KEY,KeyStroke.getKeyStroke(KeyEvent.VK_Z,ActionEvent.CTRL_MASK|ActionEvent.SHIFT_MASK,true));
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        try{
            undoManager.redo();
        }
        catch(CannotRedoException cre){
            System.out.println("Unable to redo:"+cre);
            cre.printStackTrace();
        }
        updateStatus();
        if(undo!=null){
            undo.updateStatus();
        }
    }
    
    protected void updateStatus(){
        this.setEnabled(undoManager.canRedo());
        String name;
        if(enabled)
            name=undoManager.getRedoPresentationName();
        else
            name="Redo";
        putValue(Action.NAME,name);
    }
    /**Linkea esta a una opcion undo que se le pase
     * @param undoAction la accion que rehace lo que esta deshaga
     */
    public void linkUndo(UndoAction undoAction){
        undo=undoAction;
        undo.redo=this;
    }
}
