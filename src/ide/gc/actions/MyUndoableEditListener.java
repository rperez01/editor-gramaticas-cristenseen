/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ide.gc.actions;

import ide.gc.actions.wordAnalizing.WordUndoableEdit;
import javax.swing.event.DocumentEvent;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.AbstractDocument;
import javax.swing.undo.UndoManager;
import javax.swing.undo.UndoableEdit;

/**
 * Actualiza redoAction y undoaction cuando se produce una accion editable.
 * @author Ricardo Pérez Cortés
 */
public class MyUndoableEditListener implements UndoableEditListener{
    private UndoAction ua;
    private RedoAction ra;
    private UndoManager um;

    private WordUndoableEdit lastWord;
    private static MyUndoableEditListener instance;
    /**Patron singleton
     * Devuelve la unica instancia de undoable edit listener
     * @return la unica instancia de undoable edit listener
     */
    public static MyUndoableEditListener getInstance(){
        if(instance == null){
            instance = new MyUndoableEditListener();
        }
        return instance;
    }
    /**Patron singleton
     * Devuelve la unica instancia de undoable edit listener
     * ademas de actualizar el undoManager 
     * @param um el undo manager al que engancharo
     * @return la unica instancia de undoable edit listener
     */
    public static MyUndoableEditListener getInstance(UndoManager um){
        if(instance == null){
            instance = new MyUndoableEditListener();
        }
        instance.um=um;
        if(instance.ra!=null)
            instance.ra.setUndoManager(um);
        if(instance.ua!=null)
            instance.ua.setUndoManager(um);
        return instance;
    }
    
    /**
     * Añade la undoAction a este listener
     * @param undoAction la undoAction a añadir
     * @return la instancia para encadenar llamadas
     */
    public MyUndoableEditListener set(UndoAction undoAction){
        this.ua=undoAction;
        return this;
    }
    /**
     * Añade la redoAction a este listener
     * @param redoAction la redoAction a añadir
     * @return la instancia para encadenar llamadas
     */
    public MyUndoableEditListener set(RedoAction redoAction){
        this.ra=redoAction;
        return this;
    }
    

    
    @Override
    public void undoableEditHappened(UndoableEditEvent e) {
        UndoableEdit edit =e.getEdit();
        if(um==null)
            return;
            WordUndoableEdit wue;
            if(!((AbstractDocument.DefaultDocumentEvent) edit).getType().equals(DocumentEvent.EventType.CHANGE)){
                wue= WordUndoableEdit.getWordEdit(lastWord, edit);
                if(wue==null){
                    um.addEdit(edit);
                    lastWord=wue;
                }
                else if(wue != lastWord){
                    lastWord=wue;
                    um.addEdit(wue);
                    if(ua!=null)
                        ua.updateStatus();
                    if(ra!=null)
                        ra.updateStatus();

                }
        }
    }

    public UndoManager getUndoManager() {
        return um;
    }
    
}
