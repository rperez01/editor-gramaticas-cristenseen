/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ide.gc.actions.wordAnalizing;

import JFlexGeneratedFiles.JFlexHighlightener;
import static JFlexGeneratedFiles.JFlexHighlightener.*;
import TextChunkDividers.TextChunkDivider;
import ide.gc.actions.MyUndoableEditListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import javax.swing.text.StyledDocument;



/**
 * Se encarga de resaltar un documento mientras se va escribiendo.
 * @author Ricardo Pérez Cortés
 */
public class JavaDocumentHighlightener implements DocumentListener{
    JavaWordHighlightener jwhl=null;
    JFlexHighlightener jfhl;
    AbstractDocument doc;
    TextChunkDivider tcd;
    public JavaDocumentHighlightener(AbstractDocument d){
        doc=d;
        try {
            jwhl= new JavaWordHighlightener();
            jwhl.setStyledDocument((StyledDocument) doc);
            tcd=new TextChunkDivider();
            doc.addDocumentListener(this);
            jfhl=new JFlexHighlightener(null,jwhl,tcd,0);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(JavaDocumentHighlightener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * Resets the document and sets it to the newText
     * @param newText the text the document is going to display
     */
    public void set(String newText){
        AbstractDocument d;
        MyUndoableEditListener instance;
        d=(AbstractDocument) doc;
        JavaWordHighlightener aux;
        UndoableEditListener [] uel=((AbstractDocument)doc).getUndoableEditListeners();
        aux=jwhl;
        jwhl=null;
        try {
            tcd.clear();
            doc.remove(0, doc.getLength());
            jwhl=aux;
            doc.insertString(0, newText, null);
        } catch (BadLocationException ex) {
            Logger.getLogger(JavaDocumentHighlightener.class.getName()).log(Level.SEVERE, null, ex);
        }
        instance= MyUndoableEditListener.getInstance();
            instance.getUndoManager().discardAllEdits();
        
        
        addColor.start();
        
    }

    
    @Override
    public void insertUpdate(DocumentEvent e) {

        if(jwhl!=null){
           tcd.insertUpdate(e);
           int start,end;
           boolean comment;
           start=tcd.getPreviousMarkPosition(e.getOffset());
           if(start==-1)
               start=0;
           comment=tcd.isInside(start);
           end=tcd.getNextMarkPosition(e.getOffset()+e.getLength());
            try {
                //System.out.print(start+" ");
                //System.out.print(end+" ");
                //System.out.println(doc.getLength());
                if(end!=-1)
                    jfhl.yyreset(new StringReader (doc.getText(start,end-start)),start);
                else
                    jfhl.yyreset(new StringReader (doc.getText(start,doc.getLength()-start)),start);
                if(comment)
                    jfhl.yybegin(COMENTMULTI);
                else
                    jfhl.yybegin(YYINITIAL);
                jfhl.yylex();
                if(jfhl.needsReRead){
                    int oldStart = start;
                    start=tcd.getPreviousMarkPosition(e.getOffset());
                    if(start==-1)
                        start=0;
                    comment=tcd.isInside(start);
                    int oldEnd = end;
                    end = tcd.getNextMarkPosition(e.getOffset()+e.getLength());
                    if(oldEnd!=end||oldStart!=start){
                    addColor.reset();
                        if(end!=-1)
                            jfhl.yyreset(new StringReader (doc.getText(start,end-start)),start);
                        else
                            jfhl.yyreset(new StringReader (doc.getText(start,doc.getLength()-start)),start);
                    }
                    if(comment)
                    jfhl.yybegin(COMENTMULTI);
                        else
                    jfhl.yybegin(YYINITIAL);
                    jfhl.yylex();
                }
                addColor.start();
            } catch (BadLocationException ex) {
                Logger.getLogger(JavaDocumentHighlightener.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(JavaDocumentHighlightener.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        if(jwhl!=null){
            tcd.removeUpdate(e);
           int start,end;
           boolean comment;
           start=tcd.getPreviousMarkPosition(e.getOffset()-1);
           if(start==-1)
               start=0;
           comment=tcd.isInside(start);
           end=tcd.getNextMarkPosition(e.getOffset());
            try {
               // System.out.print(start+" ");
               // System.out.print(end+" ");
                //System.out.println(doc.getLength());
                if(end!=-1)
                    jfhl.yyreset(new StringReader (doc.getText(start,end-start)),start);
                else
                    jfhl.yyreset(new StringReader (doc.getText(start,doc.getLength()-start)),start);
                if(comment)
                    jfhl.yybegin(COMENTMULTI);
                else
                    jfhl.yybegin(YYINITIAL);
                jfhl.yylex();
                addColor.start();
            } catch (BadLocationException ex) {
                Logger.getLogger(JavaDocumentHighlightener.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(JavaDocumentHighlightener.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void changedUpdate(DocumentEvent e) {

    }
}
