/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ide.gc.actions.wordAnalizing;

/**
 *
 * @author usuario
 */
class WordSeparator {
    
    /**
     * Dvuelve si un caracter es un caracter no es parte de una palabra
     * @param car el carácter que analizar
     * @return True si no es parte de una palabra, falso en caso contrario
     */
    public static boolean isWordSeparator(char car) {
        boolean ret;
        ret=car<'A'|car>'Z';
        ret&=car<'a'|car>'z';
        ret&=car<'0'|car>'9';
        ret&=car!='&';
        ret&=car!='_';
        return ret;
    }
    
}
