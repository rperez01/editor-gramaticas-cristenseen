/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ide.gc.actions.wordAnalizing;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.DocumentEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AbstractDocument.DefaultDocumentEvent;
import javax.swing.text.BadLocationException;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEdit;

/**
 * Encapsula una palabra para facilitar su edición
 * @author Ricardo Pérez Cortés
 */
public class WordUndoableEdit extends AbstractUndoableEdit{
    private String word;
    private AbstractDocument doc;
    private int offset;
    private boolean justUpdated;
    private WordUndoableEdit(int offset, AbstractDocument doc, String word){
        super();
        this.doc=doc;
        
        this.offset=offset;
        this.word=word;
        justUpdated=false;
    }
    
    /**Crea una nueva palabra editable a partir del UndoableEdit 
     * o actualiza la palabra anterior si es posible
     * 
     * @param previusWord la palabra anterior (puede ser null)
     * @param e el edit del que crear la palabra
     * @return null si no ha sido posible, la palabra anterior si se a actualizao o la nueva palabra si se ha creado
     */
    public static WordUndoableEdit getWordEdit(WordUndoableEdit previusWord, UndoableEdit e){
        AbstractDocument.DefaultDocumentEvent event;
        String next;
        int eventOff;
        try {
            if(e instanceof AbstractDocument.DefaultDocumentEvent){
                event = (AbstractDocument.DefaultDocumentEvent) e;
                if(event.getType()== DocumentEvent.EventType.INSERT && event.getLength()==1){
                    eventOff=event.getOffset();
                    if(previusWord!=null && previusWord.canUndo() && previusWord.doc.equals(event.getDocument())){
                        if(eventOff==previusWord.offset+previusWord.word.length()){

                                next = previusWord.doc.getText(eventOff, 1);
                                if (!WordSeparator.isWordSeparator(next.charAt(0))){
                                    previusWord.word+=next;
                                    return previusWord;
                                }
                        }
                    }
                    else{
                        
                        next = event.getDocument().getText(eventOff, 1);
                        if(!WordSeparator.isWordSeparator(next.charAt(0)))
                            return new WordUndoableEdit(event.getOffset(), (AbstractDocument) event.getDocument(),next);
                    }
                }
            }
        } catch (BadLocationException ex) {
        }

        return null;
    }
    
    @Override
    public void undo()throws CannotUndoException{
        if (!canUndo()) {
            throw new CannotUndoException();
        }
        UndoableEditListener [] uel=doc.getUndoableEditListeners();
        try{

            for (UndoableEditListener u  :uel){
                doc.removeUndoableEditListener(u);}
            doc.remove(offset, word.length());
            super.undo();
        
        }   catch (BadLocationException ex) {
            try {
                throw new CannotUndoException().initCause(ex);
            } catch (Throwable ex1) {
            }
        }
        
            for (UndoableEditListener u  :uel){
                doc.addUndoableEditListener(u);}
        
    }
    
    @Override
    public void redo() throws CannotRedoException{
        if (!canRedo()) {
            throw new CannotRedoException();
        }
        UndoableEditListener [] uel=doc.getUndoableEditListeners();
        try{

            for (UndoableEditListener u  :uel){
                doc.removeUndoableEditListener(u);}
            doc.insertString(offset,word,null);
            super.redo();
        
        }   catch (BadLocationException ex) {
            try {
                throw new CannotRedoException().initCause(ex);
            } catch (Throwable ex1) {
            }
            

        
        }
        for (UndoableEditListener u  :uel){
        doc.addUndoableEditListener(u);}
    }
}
