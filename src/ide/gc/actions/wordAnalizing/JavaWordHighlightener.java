/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ide.gc.actions.wordAnalizing;

import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Scanner;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

/**
 *
 * @author usuario
 */
public class JavaWordHighlightener {
    private HashSet <String> keywords;
    private static final int INIT_LOAD = 80;
    private StyledDocument doc;
    private static final Color KEYWORD_COLOR = new Color(10,30,240);
    private static final Color NORMAL_COLOR = Color.gray;
    private static final Color NUMBER_COLOR = new Color(184,67,226);//new Color(194,110,206);
    private static final Color COMMENT_COLOR = new Color(132,209,42);//new Color(230,240,35);
    private static final Color STRING_COLOR = new Color(250,127,4);//new Color(255,140,0);
    private static final Color CHAR_COLOR = new Color(250,127,4);//new Color(255,140,0);
    private static final Color SIMBOL_COLOR = new Color(125,0,0);
    
    private static  MutableAttributeSet attrNormal;
    private static  MutableAttributeSet attrKeyword; 
    private static  MutableAttributeSet attrNumber;
    private static  MutableAttributeSet attrSimbol;
    private static  MutableAttributeSet attrString;
    private static  MutableAttributeSet attrChar;
    private static  MutableAttributeSet attrComment;

    private int lastPlace; 
    private static final String FILE_LOCATION = "./src/Resources/JavaReservedWords";
    public JavaWordHighlightener() throws FileNotFoundException{
        
        keywords =new HashSet(INIT_LOAD);
        Scanner sc = new Scanner(new File(FILE_LOCATION));
        String keyword;
        while(sc.hasNext()){
            keyword=sc.nextLine();
            keyword=keyword.trim();
            keywords.add(keyword);
        }
        if(attrSimbol==null){
            attrSimbol=new SimpleAttributeSet();
            StyleConstants.setForeground(attrSimbol, SIMBOL_COLOR);
        }
        if(attrNormal==null){
            attrNormal=new SimpleAttributeSet();
            StyleConstants.setForeground(attrNormal, NORMAL_COLOR);
        }
        if(attrKeyword==null){
            attrKeyword=new SimpleAttributeSet();
            StyleConstants.setForeground(attrKeyword, KEYWORD_COLOR);
        }
        if(attrChar==null){
            attrChar=new SimpleAttributeSet();
            StyleConstants.setForeground(attrChar, CHAR_COLOR);
        }
        if(attrString==null){
            attrString=new SimpleAttributeSet();
            StyleConstants.setForeground(attrString, STRING_COLOR);
        }
        if(attrNumber==null){
            attrNumber=new SimpleAttributeSet();
            StyleConstants.setForeground(attrNumber, NUMBER_COLOR);
        }
        if(attrComment==null){
            attrComment=new SimpleAttributeSet();
            StyleConstants.setForeground(attrComment, COMMENT_COLOR);
        }
    }
    
    /**Comprueba que una palabra es un keyword y si lo es la resalta
     * @param offset la distancia del documento a la que está la palabra
     * @param word la palabra que se va a intentar resaltar
     */
    public void highlightKeyWord(int offset, String word){
        if(keywords.contains(word)){
            
            new addColor(lastPlace, offset-lastPlace, attrNormal, doc);
            new addColor(offset, word.length(), attrKeyword, doc);
            lastPlace=offset+word.length();
        }
        else{
            new addColor(lastPlace, offset+word.length()-lastPlace, attrNormal, doc);
            lastPlace=offset+word.length();
        }
        
    }
    
    /**Resalta un comentario
     * @param offset la distancia del documento a la que está la palabra
     * @param length el numero de caracteres que ocupa el numero
     */
    public void highlightComment(int offset, int length){
            new addColor(lastPlace, offset-lastPlace, attrNormal, doc);
            new addColor(offset, length, attrComment, doc);
            lastPlace=offset+length;
    }
    
    
    /**Resalta una referencia a un simbolo
     * @param offset la distancia del documento a la que está la palabra
     * @param length el numero de caracteres que ocupa el numero
     */
    public void highlightSimbol(int offset, int length){
        new addColor(lastPlace, offset-lastPlace, attrNormal, doc);
        new addColor(offset, length, attrSimbol, doc);            
        lastPlace=offset+length;
    }

        
    /**Resalta un numero
     * @param offset la distancia del documento a la que está la palabra
     * @param length el numero de caracteres que ocupa el numero
     */
    public void highlightNumber(int offset, int length){
            new addColor(lastPlace, offset-lastPlace, attrNormal, doc);
            new addColor(offset, length, attrNumber, doc);
            lastPlace=offset+length;
    }
    
     /**Resalta un string
     * @param offset la distancia del documento a la que está la palabra
     * @param length el numero de caracteres que ocupa el numero
     */
    public void highlightString(int offset, int length){
            new addColor(lastPlace, offset-lastPlace, attrNormal, doc);
            new addColor(offset, length, attrString, doc);
            lastPlace=offset+length;
    }
    
    /**Resalta un caracter
     * @param offset la distancia del documento a la que está la palabra
     * @param length el numero de caracteres que ocupa el numero
     */
    public void highlightChar(int offset, int length){
            new addColor(lastPlace, offset-lastPlace, attrNormal, doc);
            new addColor(offset, length, attrChar, doc);
            lastPlace=offset+length;
    }
    /**
     * Indica cuando finalizar el resaltado
     * @param offset el lugar donde finaliza el resaltado
     */
    public void highlightFinish(int offset){
        new addColor(lastPlace, offset-lastPlace, attrNormal, doc);
        lastPlace=offset;
    }
    /**
     * Indica cuando empezar el resaltado
     * @param offset el lugar donde finaliza el resaltado
     */
    public void highlightStart(int offset){
        lastPlace=offset;
    }
            
    public void setStyledDocument(StyledDocument doc){
        this.doc=doc;
    }
    
    public  StyledDocument getStyledDocument(){
        return doc;
    }
    
    
}
