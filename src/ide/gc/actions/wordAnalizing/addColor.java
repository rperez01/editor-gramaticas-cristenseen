/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ide.gc.actions.wordAnalizing;

import java.util.LinkedList;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.AbstractDocument;
import javax.swing.text.StyledDocument;
import javax.swing.text.MutableAttributeSet;

/**
 * Hace la puta funcion en un thread
 * @author usuario
 */
public class addColor {
    private StyledDocument doc;
    private final int color_init;
    private final int color_offset;
    private final MutableAttributeSet color_attr; 
    private final static LinkedList<addColor> cola = new LinkedList();
    public addColor(int cinit, int coffset, MutableAttributeSet cattr, StyledDocument d){
        doc=d;
        color_attr=cattr;
        color_offset=coffset;
        color_init=cinit;
        try {
            QueueEmptier.sem.acquire();
            cola.addLast(this);
        } catch (InterruptedException ex) {
            Logger.getLogger(addColor.class.getName()).log(Level.SEVERE, null, ex);
        }
        QueueEmptier.sem.release();

    }
    
    public static void start(){
        new QueueEmptier();
    }
    public static void reset(){
        try {
            QueueEmptier.sem.acquire();
        } catch (InterruptedException ex) {
            Logger.getLogger(addColor.class.getName()).log(Level.SEVERE, null, ex);
        }
             cola.clear();
        QueueEmptier.sem.release();
    }
    
    protected static class QueueEmptier extends Thread{
        private final static Semaphore sem=new Semaphore(1);
        protected QueueEmptier(){
            this.start();
        }

        @Override
        public void run(){
            addColor ac;

            try {
                sem.acquire();
                while(!cola.isEmpty()){
                    
                    ac = cola.removeLast();
                    
                    ac.doc.setCharacterAttributes(ac.color_init, ac.color_offset, ac.color_attr, false);
                }
                sem.release();
            } catch (InterruptedException ex) {
                Logger.getLogger(addColor.class.getName()).log(Level.SEVERE, null, ex);
            }
            QueueEmptier.sem.release();
        }
    }
}
