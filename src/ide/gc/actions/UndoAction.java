/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ide.gc.actions;

import java.awt.event.KeyEvent;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.KeyStroke;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;

/**
 * Intenta rehacer una acción
 * @author Ricardo Pérez Cortés
 */
public class UndoAction extends AbstractAction{
    protected UndoManager undoManager;
    protected RedoAction redo;
    
    public void setUndoManager(UndoManager undoManager){
        this.undoManager=undoManager;
        if(undoManager!=null){
            this.updateStatus();
        }
        else{
            setEnabled(false);
            putValue(Action.NAME,"Redo");
        }
    }
    
    public UndoAction(){
        super();
        if(undoManager==null){
            putValue(Action.NAME,"Undo");
            setEnabled(false);
        }
        else
            updateStatus();
        this.putValue(ACCELERATOR_KEY,KeyStroke.getKeyStroke(KeyEvent.VK_Z,ActionEvent.CTRL_MASK,true));
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        try{
            undoManager.undo();
        }
        catch(CannotUndoException cre){
            System.out.println("Unable to undo:"+cre);
            cre.printStackTrace();
        }
        updateStatus();
        if(redo!=null){
            redo.updateStatus();
        }
    }
    
    protected void updateStatus(){
        this.setEnabled(undoManager.canUndo());
        String name;
        if(enabled)
            name=undoManager.getUndoPresentationName();
        else
            name="Undo";
        putValue(Action.NAME,name);
    }
    
    /**Linkea esta a una opcion redo que se le pase
     * @param redoAction la accion que rehace lo que esta deshaga
     */
    public void linkRedo(RedoAction redoAction){
        redo=redoAction;
        redo.undo=this;
    }
}
