/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ExternalComunication;

import java.util.ArrayList;

/**
 * Represents a syntactic rule
 * @author Ricardo Pérez cortés
 */
public class SyntaxRule  {
    private final SyntaxSymbolNonTerminal parteIzquierda;
    private final ArrayList<SyntaxSymbol> parteDerecha;
    
    public SyntaxRule(SyntaxSymbolNonTerminal parteIzquierda){
        parteDerecha = new ArrayList();
        this.parteIzquierda=parteIzquierda;
    }
    
    public void AddRightPartSymbol (SyntaxSymbol symbol) {
        parteDerecha.add(symbol);
    } 
    
    public void AddRightPartSymbol (int i,SyntaxSymbol symbol){
        parteDerecha.add(i,symbol);
    }
    
    public SyntaxSymbol GetRightPartSymbol(int i){
        return parteDerecha.get(i);
    }
    
    public SyntaxSymbol GetLeftPartSymbol(){
        return parteIzquierda;
    }
    
    public void RemoveRightPartSymbol(int i){
        parteDerecha.remove(i);
    }
    
    public void RemoveRightPartSymbol(SyntaxSymbol symbol){
        parteDerecha.remove(symbol);
    }
    
    public int RightPartSize(){
        return parteDerecha.size();
    }
    @Override
    public String toString(){
        String ret ;
        ret =this.parteIzquierda.toString()+":";
        for(SyntaxSymbol steve:  this.parteDerecha){
            ret+=" "+steve.toString();
        }
        return ret;
    }

    
    
    @Override
    public boolean equals(Object obj){
        SyntaxRule s =(SyntaxRule) obj;
        if(s == null)
            return false;
        return s.parteIzquierda.equals(parteIzquierda)&&s.parteDerecha.equals(parteDerecha);
    }
    
}
