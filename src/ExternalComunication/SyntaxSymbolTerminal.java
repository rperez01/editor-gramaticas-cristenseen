/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ExternalComunication;

/**
 * Represents a terminal syntax symbol
 * @author ricardo
 */
public class SyntaxSymbolTerminal extends SyntaxSymbol {
    
    public SyntaxSymbolTerminal(String symbol){
        super(symbol);
        
    }

    @Override
    public boolean Terminal() {
        return true;
    }
}
