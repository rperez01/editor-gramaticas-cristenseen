/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ExternalComunication;

import gemma_esqrew.CadenaSimbolos;
import gemma_esqrew.GIC;
import gemma_esqrew.NoTerminal;
import gemma_esqrew.Regla;
import gemma_esqrew.Terminal;
import java.io.FileNotFoundException;

/**
 *Clase encargada de convertir un syntax holder en GIC
 * @author Ricardo Perez Cortés
 */
public class SyntaxProxy {
    
    public static GIC SyntaxHolderToGIC(SyntaxHolder sh){
        GIC ret;
        NoTerminal parteIzquierda;
        CadenaSimbolos parteDerecha;
        Regla r;
        ret= new GIC();
        //añadimos los no terminales (las comprobaciones necesarias ya estaban hechas en SyntaxHolder)
        for(SyntaxSymbol ss:sh.GetNonTerminalSymbolArray()){
            ret.anadirNoTerminal(ss.toString());
        }
        //Añadimos los terminales (las comprobaciones necesarias ya estaban hechas en SyntaxHolder)
        for(SyntaxSymbol ss:sh.GetTerminalSymbolArray()){
            ret.anadirTerminal(ss.toString());
        }
        //Creamos y añadimos las reglas
        int aux = sh.RulesSize();
        SyntaxRule sr;
        
        for(int i = 0;i<aux;i++){
            SyntaxSymbol ss;
            sr=sh.GetRule(i);
            parteIzquierda = new NoTerminal(sr.GetLeftPartSymbol().toString());
            parteDerecha = new CadenaSimbolos();
            int aux2= sr.RightPartSize();
            for(int j=0; j<aux2; j++){
                ss=sr.GetRightPartSymbol(j);
                if(ss.Terminal())
                    parteDerecha.anadirSimbolo(new Terminal(ss.toString()));
                else
                    parteDerecha.anadirSimbolo(new NoTerminal(ss.toString()));
            }
            r=new Regla(parteIzquierda,parteDerecha);
            ret.anadirRegla(r);
            
        }
        //definimos el axioma
        ret.definirAxioma(new NoTerminal(sh.GetAxiom().toString()));
        return ret;
    }
    
    public static GIC JSONFileToGIC(String path) throws FileNotFoundException{
        SyntaxHolder sh=new SyntaxHolder();
        sh.LoadFromFile(path);
        return SyntaxHolderToGIC(sh);
    }
    
}