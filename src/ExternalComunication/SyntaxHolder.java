/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ExternalComunication;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Designed for  syntax intercommunication, holds the syntax
 * rules and can store and read from a file
 * @author Ricardo Pérez Cortés
 */
public class SyntaxHolder {
    private ArrayList<SyntaxRule> rules;
    private SyntaxSymbolNonTerminal axioma;
    private HashSet<SyntaxSymbolNonTerminal> noTerminales;
    private HashSet<SyntaxSymbolTerminal> terminales;
    
    
    public void SetAxiom(SyntaxSymbolNonTerminal axioma){
        this.axioma=axioma;
        noTerminales.add(axioma);
    }
    
    public SyntaxSymbolNonTerminal GetAxiom(){
        return axioma;
    }
    
    
    
    public SyntaxHolder(){
        axioma=null;
        rules = new ArrayList<>();
        noTerminales= new HashSet();
        terminales =  new HashSet();
        terminales.add(new SyntaxSymbolTerminal(""));
    }
    
    
    //Setters y getters
    public void AddRule (SyntaxRule symbol) {
        if(axioma==null)
            axioma=(SyntaxSymbolNonTerminal) symbol.GetLeftPartSymbol();
        rules.add(symbol);
    } 

    public void AddRule (int i,SyntaxRule symbol){
        rules.add(i,symbol);
    }
    
    public SyntaxRule GetRule(int i){
        return rules.get(i);
    }
    
    public int GetRulePosition(SyntaxRule sr){
        return rules.indexOf(sr);
    }
    

    public void RemoveRule(int i){
        rules.remove(i);
    }
    
    public void RemoveRule(SyntaxRule symbol){
        rules.remove(symbol);
    }
    
    public int RulesSize(){
        return rules.size();
    }
    
    public void AddSymbol (SyntaxSymbolTerminal symbol) {
        if(noTerminales.contains(new SyntaxSymbolNonTerminal(symbol.symbol)))
            return;
        terminales.add(symbol);
    } 

    public void AddSymbol (SyntaxSymbolNonTerminal symbol) {
        if(terminales.contains(new SyntaxSymbolTerminal(symbol.symbol)))
            return;
        noTerminales.add(symbol);
    } 
    
    public void RemoveSymbol (SyntaxSymbolTerminal symbol){
        terminales.remove(symbol);
    }
    
    public void RemoveSymbol (SyntaxSymbolNonTerminal symbol){
        noTerminales.remove(symbol);
    }
    
    public boolean ContainsSymbol (SyntaxSymbolTerminal symbol){
        return terminales.contains(symbol);
    }
    
    public boolean ContainsSymbol (SyntaxSymbolNonTerminal symbol){
        return noTerminales.contains(symbol);
    }
    
    public SyntaxSymbolTerminal[] GetTerminalSymbolArray(){
        return terminales.toArray(new SyntaxSymbolTerminal[terminales.size()]);
    }
    
    public SyntaxSymbolNonTerminal[] GetNonTerminalSymbolArray(){
        return  noTerminales.toArray(new SyntaxSymbolNonTerminal[noTerminales.size()]);
    }
    
    //Funciones mas rarunas
    
    /**
     * Checks if every rule uses symbols that are in the lists
     * @return if is consistent this holder
     */ 
    public boolean CheckConsistency(){
        for(SyntaxRule rule :rules){
            if(!noTerminales.contains(rule.GetLeftPartSymbol()))
                return false;
            for(int i = 0 ; i<rule.RightPartSize();i++){
                SyntaxSymbol symbol;
                symbol =  rule.GetRightPartSymbol(i);
                if(symbol.Terminal()){
                    if(!terminales.contains(symbol))
                        return false;
                }
                else{
                    if(!noTerminales.contains(symbol))
                        return false;
                }
            }
        }
        return true;
    }
    
    /**
     * Saves this syntax into a file
     * @param path the path to the file
     */
    public void SaveToFile(String path) throws FileNotFoundException{
        Gson jsonlib=new Gson();
        String thisJsonized;
        thisJsonized=jsonlib.toJson(this);
        Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path)));
        try {
            out.write(thisJsonized);
            out.close();
        } catch (IOException ex) {
            Logger.getLogger(SyntaxHolder.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * Loads this syntax from a file
     * @param path the path to the file
     */
    public void LoadFromFile(String path) throws FileNotFoundException{
        Gson jsonlib=new Gson();
        SyntaxHolder aux;
        String thisJsonized;
        Reader in = new BufferedReader(new InputStreamReader(new FileInputStream(path)));
        aux=jsonlib.fromJson(in, this.getClass());
        this.noTerminales=aux.noTerminales;
        this.terminales=aux.terminales;
        this.rules= aux.rules;
    }
    /**
     * Tries to make a sintax rule from a string using the simbols in the holder
     * @param rule the rule using this format <NonTerminalSymbol>  : <TerminalOrNonTerminalSymbol> <TerminalOrNonTerminalSymbol> ...
     * @return the syntax rule if it could be maked or null otherwise
     */
    public SyntaxRule SyntaxRuleFromString(String rule){
       SyntaxRule sr;
       String[] aux;
       SyntaxSymbolNonTerminal nts;
       SyntaxSymbolTerminal ts;
       String sym;
       aux=rule.split(":");
       if(aux.length!=2)
           return null;
       sym=aux[0].trim();
       nts=new SyntaxSymbolNonTerminal (sym);
       if(!noTerminales.contains(nts))
           return null;
       sr= new SyntaxRule(nts);
       aux=aux[1].trim().split(" ");
       for(String symbol : aux){
           nts=new SyntaxSymbolNonTerminal(symbol);
           if(noTerminales.contains(nts)){
               sr.AddRightPartSymbol(nts);
           }
           else{
               ts= new SyntaxSymbolTerminal(symbol);
               if(terminales.contains(ts)){
                   sr.AddRightPartSymbol(ts);
               }
               else
                   return null;
           }
       }
       
       return sr;
    }
    
    
}
