/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ExternalComunication;

import java.util.Objects;

/**
 * Simbolo sintactico basico
 * @author Ricardo Pérez Cortés
 */
public abstract class SyntaxSymbol {
    protected final String symbol;
    
    SyntaxSymbol(String symbol){
       this.symbol=symbol;
    }
    

    @Override
    public boolean equals(Object obj){
        return obj.getClass()==this.getClass()&& symbol.equals(((SyntaxSymbol)obj).symbol);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.symbol);
    }
    
    @Override
    public String toString(){
        return symbol;
    }
    
    public abstract boolean Terminal();
    
}
