
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ExternalAuxiliarPrograms;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import jflex.SilentExit;

/**
 *  Crea archivos lexicos y de sintaxis a partir de jflex y jcup
 * @author Ricardo Perez Cortés
 */
public class JFlexCompiler {

    //public static final String destinoJcup="src/Analizador";
    public static final String file1="./src/JFlexSourceFiles/JFlexHighlightener.flex";
    public static final String file2="./src/JFlexSourceFiles/JFlexRenamer.flex";
    public static final String OutputDir="./src/JFlexGeneratedFiles";
    public static void main(String[] args) throws SilentExit {
        jflex.Main.parseOptions(new String [] {"-d",OutputDir});
        jflex.Main.generate(new File(file1));
        jflex.Main.generate(new File(file2));
        
    }
}
