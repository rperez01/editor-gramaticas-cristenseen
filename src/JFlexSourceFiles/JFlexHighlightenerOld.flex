


package JFlexGeneratedFiles;

import ide.gc.actions.wordAnalizing.JavaWordHighlightener;
%%
%public
%class JFlexHighlightener
%char
%integer


%{
    /*public static boolean justReadALine = false;*/
    private  int startPos = 0;
    JavaWordHighlightener hl;
    private int init;
    
    public JFlexHighlightener (java.io.Reader r,JavaWordHighlightener highlightener, int startingPos){
        this(r);
        this.startPos=startingPos;
        this.hl=highlightener;
    }
    
    public void yyreset (java.io.Reader r,int startingPos){
        this.yyreset(r);
        this.startPos=startingPos;
    }
%}
FinDocumento = \z
FinDeLinea  = \r|\n|\r\n|\z

Caracter    = \'(.|(\\.))\'

Cifra           = [0-9]
Entero          = [1-9]{Cifra}*
Flotante        = Entero\.{Cifra}*f?
Octal           = 0{Entero}
Hexadecimal     = 0x[1-9A-Fa-f][0-9A-Fa-f]*
Zeros           = 00*
Numero          = {Hexadecimal}|{Octal}|{Flotante}|{Entero}|{Zeros}

Palabra         = [$_A-Za-z]([$_A-Za-z]|{Cifra})*

Comillas        = \"
Comentario      = \/\/
ComentMultiInit = \/\*
ComentMultiFin  = \*\/

%state STRING
%state COMENT
%state COMENTMULTI
%%
<YYINITIAL> {
{FinDocumento}      {hl.highlightFinish(startPos+yychar);}
{Numero}            {hl.highlightNumber(startPos+yychar,yylength());System.out.print("Numero:"+yytext()+"\n");}
{Palabra}           {hl.highlightKeyWord(startPos+yychar,yytext());}
{Caracter}          {hl.highlightChar(startPos+yychar,yylength());System.out.print("caracter:"+yytext()+"\n");}
{FinDeLinea}        {}
{Comillas}          {init=yychar;
                    yybegin(STRING);}
{Comentario}        {init=yychar;
                    yybegin(COMENT);}
{ComentMultiInit}   {
                    System.out.print("Comentario detectado\n");
                    init=yychar;
                    yybegin(COMENTMULTI);}
.                   {}
}

<STRING> {
{Comillas}|{FinDeLinea}          {hl.highlightString(init+startPos,yychar+yylength());
                    yybegin(YYINITIAL);}   
.                   {}
}

<COMENT> {
{FinDeLinea}        {hl.highlightComment(init+startPos,yychar+yylength());
                    yybegin(YYINITIAL);}
.                   {}
}

<COMENTMULTI> {
{ComentMultiFin}|{FinDocumento}    {hl.highlightComment(init+startPos,yychar+yylength());
                    System.out.print("Fin de comentario:"+(init+startPos)+" "+(yychar+yylength())+"\n");
                    yybegin(YYINITIAL);}
{FinDeLinea}        {}
.                   {}
}


