

package JFlexGeneratedFiles;
import TextChunkDividers.*;
import ide.gc.actions.wordAnalizing.JavaWordHighlightener;
%%
%public
%class JFlexHighlightener
%char
%integer


%{
    /*public static boolean justReadALine = false;*/
    public boolean needsReRead = false;
    private  int startPos = 0;
    JavaWordHighlightener hl;
    private int init;
    private TextChunkDivider tcd;

    public JFlexHighlightener (java.io.Reader r,JavaWordHighlightener highlightener, TextChunkDivider tcd,int startingPos){
        this(r);
        this.startPos=startingPos;
        this.hl=highlightener;
        this.tcd=tcd;
        hl.highlightStart(startingPos);
    }
    
    public void yyreset (java.io.Reader r,int startingPos){
        this.yyreset(r);
        this.startPos=startingPos;
        hl.highlightStart(startingPos);
    }
%}
FinDeLinea  = \r|\n|\r\n

Caracter    = \'(.|(\\.))\'

Cifra           = [0-9]
Entero          = [1-9]{Cifra}*
Flotante        = Entero\.{Cifra}*f?
Octal           = 0{Entero}
Hexadecimal     = 0x[1-9A-Fa-f][0-9A-Fa-f]*
Zeros           = 00*
Numero          = {Hexadecimal}|{Octal}|{Flotante}|{Entero}|{Zeros}

Simbolo         = (\${Cifra}+)|(\$\$)

SimboloSintactico   =   \$\(

Palabra         = [$_A-Za-z]([$_A-Za-z]|{Cifra})*

Comillas        = \"
Comentario      = \/\/
ComentMultiInit = \/\*
ComentMultiFin  = \*\/

%state STRING
%state COMENT
%state COMENTMULTI
%state SS
%%
<YYINITIAL> {
<<EOF>>             {hl.highlightFinish(startPos+yychar);
                    needsReRead=false;
                    return(YYEOF);

                    }
{Numero}            {hl.highlightNumber(startPos+yychar,yylength());/*System.out.print("Numero:"+yytext()+"\n");*/}
{Simbolo}           {hl.highlightSimbol(startPos+yychar,yylength());/*System.out.print("Simbolo:"+yytext()+"\n");*/}
{SimboloSintactico} {hl.highlightSimbol(startPos+yychar,yylength());
                    init=yychar+yylength();
                    yybegin(SS);}
{Palabra}           {hl.highlightKeyWord(startPos+yychar,yytext());}
{Caracter}          {hl.highlightChar(startPos+yychar,yylength());/*System.out.print("caracter:"+yytext()+"\n");*/}
{FinDeLinea}        {tcd.insertMark(startPos+yychar, new EndLineMark());}
{Comillas}          {init=yychar;
                    yybegin(STRING);}
{Comentario}        {init=yychar;
                    yybegin(COMENT);}
{ComentMultiInit}   {
                    
                    //System.out.print("Comentario detectado\n");
                    tcd.insertMark(startPos+yychar, new MultiComentatorMark(MultiComentatorMark.TYPE.OPENING));
                    init=yychar;
                    yybegin(COMENTMULTI);}
.                   {}
}

<STRING> {
{Comillas}            {hl.highlightString(init+startPos,yychar+yylength()-init);
                    yybegin(YYINITIAL);}   
{FinDeLinea}          {hl.highlightString(init+startPos,yychar+yylength()-init);
                    yybegin(YYINITIAL);
                    tcd.insertMark(startPos+yychar, new EndLineMark());
                    }
<<EOF>>             {hl.highlightString(init+startPos,yychar+yylength()-init);
                    yybegin(YYINITIAL);
                    needsReRead=false;
                    return(YYEOF);

                    }
.                   {}
}

<COMENT> {
{FinDeLinea}        {hl.highlightComment(init+startPos,yychar+yylength()-init);
                    yybegin(YYINITIAL);
                    tcd.insertMark(startPos+yychar, new EndLineMark());
                    }
<<EOF>>             {hl.highlightComment(init+startPos,yychar+yylength()-init);
                    yybegin(YYINITIAL);
                    needsReRead=false;
                    return(YYEOF);

                    }
.                   {}
}
<SS>{
\)                  {hl.highlightString(init+startPos,yychar+yylength()-init-1);
                    hl.highlightSimbol(yychar+startPos,1);
                    yybegin(YYINITIAL);}   
{FinDeLinea}          {hl.highlightString(init+startPos,yychar+yylength()-init-1);
                    yybegin(YYINITIAL);
                    tcd.insertMark(startPos+yychar, new EndLineMark());
                    }
<<EOF>>             {hl.highlightString(init+startPos,yychar+yylength()-init);
                    yybegin(YYINITIAL);
                    needsReRead=false;
                    return(YYEOF);

                    }
.                   {}

}


<COMENTMULTI> {
{ComentMultiFin}    {hl.highlightComment(init+startPos,yychar+yylength()-init);/*
                    System.out.print("Fin de comentario:"+(init+startPos)+" "+(yychar+yylength())+"\n");*/
                    yybegin(YYINITIAL);
                    tcd.insertMark(startPos+yychar, new MultiComentatorMark(MultiComentatorMark.TYPE.CLOSING));
                    }
<<EOF>>             {hl.highlightComment(init+startPos,yychar+yylength()-init);/*
                    System.out.print("Fin de comentario:"+(init+startPos)+" "+(yychar+yylength())+"\n");*/
                    yybegin(YYINITIAL);
                    needsReRead=true;
                    return(YYEOF);

                    }
{FinDeLinea}        {}
.                   {}
}


