
package JFlexGeneratedFiles;
import ExternalComunication.*;
import ide.gc.globalvalues.SymbolsStorage;
import Compiler.CompilerException;
import java.util.ArrayList;
import Compiler.ParentesisChecker;
import Compiler.ruleBucleChecker;
import Compiler.SimpleFunction;
%%
%public
%class JFlexRenamer
%char
%integer
%line
%yylexthrow{
CompilerException
%yylexthrow}

%{
    /*public static boolean justReadALine = false;*/
    private SyntaxHolder sh;
    private SyntaxRule sr;
    private StringBuffer s;
    private String sraux;
    private int instant;
    private String symbolAtt;
    private SyntaxSymbol attProcesed;
    private ParentesisChecker pc;
    private ruleBucleChecker rbc;
    private int hijo;
    public JFlexRenamer (java.io.Reader r,SyntaxHolder sh, SyntaxRule sr, int instant,ruleBucleChecker rbc){
        this(r);
        s=new StringBuffer();
        this.sr=sr;
        this.sh=sh;
        this.instant= instant;
        this.rbc=rbc;
        pc = new ParentesisChecker();
    }
    

    @Override
    public String toString(){
        return s.toString();
    }

    public void reset (java.io.Reader r,SyntaxHolder sh, SyntaxRule sr,int instant,ruleBucleChecker rbc){
        this.yyreset(r);
        s=new StringBuffer();
        this.sr=sr;
        this.sh=sh;
        this.instant= instant;
        this.rbc=rbc;
        pc = new ParentesisChecker();
    }

    public void reset (java.io.Reader r,SyntaxHolder sh, SyntaxRule sr,int instant){
        this.yyreset(r);
        s=new StringBuffer();
        this.sr=sr;
        this.sh=sh;
        this.instant= instant;
        pc = new ParentesisChecker();
    }


    public void reset (java.io.Reader r, SyntaxRule sr,int instant){
        this.yyreset(r);
        s=new StringBuffer();
        this.sr=sr;
        this.instant= instant;
        pc = new ParentesisChecker();
    }

    public void reset (java.io.Reader r,int instant){
        this.yyreset(r);
        s=new StringBuffer();
        this.instant= instant;
        pc = new ParentesisChecker();
        
    }

    public void reset (java.io.Reader r){
        this.yyreset(r);
        s=new StringBuffer();
        pc = new ParentesisChecker();
    }

    public void setRuleBucleChecker(ruleBucleChecker rbc) {
        this.rbc=rbc;
    }
%}
FinDeLinea  = \r|\n|\r\n

Caracter    = \'(.|(\\.))\'

Cifra           = [0-9]
Entero          = [1-9]{Cifra}*
Flotante        = {Entero}\.{Cifra}*f?
Octal           = 0{Entero}
Hexadecimal     = 0x[1-9A-Fa-f][0-9A-Fa-f]*
Zeros           = 00*
Numero          = {Hexadecimal}|{Octal}|{Flotante}|{Entero}|{Zeros}

Simbolo         = \${Cifra}\.
SimboloI        = \$\$\.

SyntaxRule      = \$\(

OParentesis     =\(
CParentesis     =\)

Palabra         = [_A-Za-z]([_A-Za-z]|{Cifra})*

PalabroReservado    = (arbol)|(atributosSintetizados)|(atributosHeredadosPadre)|(atributosHeredadosHermanos)|(hijo)|(regla)|(reglasConstruidas)
SafePalabroReservado    = \.{PalabroReservado}
Comillas        = \"
Comentario      = \/\/
ComentMultiInit = \/\*
ComentMultiFin  = \*\/

%state STRING
%state COMENT
%state COMENTMULTI
%state SYNTAXRULE
%state ATTRIBUTE
%%
<YYINITIAL> {
{SafePalabroReservado}  {s.append(yytext());}
{PalabroReservado}  {s.append("_"+yytext());}

{Caracter}          {s.append(yytext());}

{Comillas}          {
                    s.append(yytext());
                    yybegin(STRING);}
{Comentario}        {
                    s.append(yytext());
                    yybegin(COMENT);}
{ComentMultiInit}   {
                    s.append(yytext());
                    yybegin(COMENTMULTI);}
{Simbolo}           {
                        String aux;
                        aux=yytext().substring(1,yylength()-1);
                        symbolAtt= "hijo"+aux;
                        hijo=Integer.parseInt(aux);
                        if(sr.RightPartSize()<=hijo)
                            throw new CompilerException("The rule does not have so many symbols",yyline,instant,sr);
                        attProcesed = sr.GetRightPartSymbol(hijo);
                        yybegin(ATTRIBUTE);
}
{SimboloI}          {
                        symbolAtt="arbol";
                        hijo=-1;
                        attProcesed = sr.GetLeftPartSymbol(); 
                        yybegin(ATTRIBUTE);
}
{SyntaxRule}          {
                        sraux="";
                        yybegin(SYNTAXRULE);
}
{OParentesis}       {s.append(yytext());pc.addOpenParentesis();}
{CParentesis}       {s.append(yytext());pc.addClosingParentesis();}

.|{FinDeLinea}                 {
                    s.append(yytext());}
}

<STRING> {
{Comillas}|{FinDeLinea}            {
                       s.append(yytext());
                       yybegin(YYINITIAL);}   

.                   {s.append(yytext());}
}

<COMENT> {
{FinDeLinea}        {s.append(yytext());
                    yybegin(YYINITIAL);
                    }
.                   {s.append(yytext());}
}

<COMENTMULTI> {
{ComentMultiFin}    {s.append(yytext());
                    yybegin(YYINITIAL);
                    }

.                   {s.append(yytext());}
                    }
<ATTRIBUTE>{

{Palabra}\(        {
                    SymbolsStorage.SymbolInfo si=SymbolsStorage.getStorage().getInfo(attProcesed.toString());
                    if(si==null)
                        throw new CompilerException("Symbol missing:"+attProcesed.toString(),yyline,instant,sr);
                    String attribute=yytext().substring(0,yylength()-1);
                    String type = si.attributes.get(attribute);
                    if(type==null)
                        throw new CompilerException("Attribute not defined:"+attribute,yyline,instant,sr);
                    int pos = new ArrayList<String>(si.attributes.keySet()).indexOf(attribute);
                    s.append(symbolAtt+".atributos.set("+pos+",");
                    attProcesed=null;
                    yybegin(YYINITIAL);
                    pc.addOpenParentesis();
                    if(hijo==-1){
                        pc.setFunctionWhenClosing(new SimpleFunction(){
                            @Override
                            public void function() {
                                rbc.setAttributeDetectedInternal(instant, pos);
                            }

                        });
                    } else
                    {
                        pc.setFunctionWhenClosing(new SimpleFunction(){
                            @Override
                            public void function() {
                                rbc.setAttributeDetected(instant, hijo, pos);
                            }

                        });
                    }
}


{Palabra}           {
                    SymbolsStorage.SymbolInfo si=SymbolsStorage.getStorage().getInfo(attProcesed.toString());
                    if(si==null)
                        throw new CompilerException("Symbol missing:"+attProcesed.toString(),yyline,instant,sr);
                    String type = si.attributes.get(yytext());
                    if(type==null)
                        throw new CompilerException("Attribute not defined:"+attProcesed.toString(),yyline,instant,sr);
                    int pos = new ArrayList<String>(si.attributes.keySet()).indexOf(yytext());
                    s.append("(("+type+") "+symbolAtt+".atributos.get("+pos+"))");
                    attProcesed=null;
                    if(hijo==-1){
                        rbc.getAttributeDetectedInternal(instant, pos);
                    } else
                    {
                        rbc.getAttributeDetected(instant, hijo, pos);
                    }
}



.                   {
                    if(attProcesed!=null)
                        throw new CompilerException("Found symbol but no attribute",yyline,instant,sr);
                    s.append(yytext());
                    yybegin(YYINITIAL);
                    }   
}

<SYNTAXRULE>{
")"     {SyntaxRule aux;
        aux=sh.SyntaxRuleFromString(sraux);
        if(aux==null)
            throw new CompilerException("Rule malformed: " +sraux,yyline,instant,sr);
        int pos = sh.GetRulePosition(aux);
        if(pos==-1)
            throw new CompilerException("Rule not found in syntax: sraux",yyline,instant,sr);
        s.append("r"+pos);
        yybegin(YYINITIAL);
        }
.       {sraux+=yytext();}

}



