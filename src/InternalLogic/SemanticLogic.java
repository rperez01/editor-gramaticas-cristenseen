/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InternalLogic;

import ide.gc.sintaxLogic.SyntaxMarker;
import ide.gc.ui.JPSemantica;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JTextPane;

/**
 * Implements the logic needed to operate the semantic part
 * @author Ricardo Pérez Cortés
 */
public class SemanticLogic implements FocusListener,ActionListener{
    private JTextPane syntaxP;
    private SyntaxMarker sm;
    private Project p;
    private SyntaxLine sl;
    private JComboBox jcb;
    public SemanticLogic (JComboBox selectors,JTextPane pane){
        syntaxP=pane;
        syntaxP.addFocusListener(this);
        JavaCode.setUnactive();
        sm=SyntaxMarker.getInstance();
        p=sm.getProject();
        jcb=selectors;
        selectors.addActionListener(this);
    }
    
    @Override
    public void focusGained(FocusEvent e) {
        JavaCode.setUnactive();
        sl=null;
        jcb.removeAllItems();
        jcb.addItem("0");
        jcb.setSelectedIndex(0);
    }

    @Override
    public void focusLost(FocusEvent e) {
        Project.Container aux = p.getLine(sm.getNewLineMarker().getNearestMarkIndex(syntaxP.getCaret().getDot()-1));
        sl=aux.line;
        int numberLines;
        if(sl!=null){
            sl.setActiveCode(0);
            jcb.removeAllItems();
            numberLines=sl.getMaxCodes();
            for(int i = 0 ; i<=numberLines;i++)
                jcb.addItem(Integer.toString(i));
            jcb.setSelectedIndex(0);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int number;
        if(sl!=null){
            number=jcb.getSelectedIndex();
            sl.setActiveCode(number);
        }
    }
    
}
