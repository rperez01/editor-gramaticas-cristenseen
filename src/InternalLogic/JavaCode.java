/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InternalLogic;

import ide.gc.ui.JPSemantica;

/**
 * Contains a chunk of code as storage
 * @author Ricardo Pérez Cortés
 */
public class JavaCode {
    
    public static void setJPSemantica(JPSemantica panel) {
        ui=panel;
    }
    
    private String code;
    private static JPSemantica ui;
    private static JavaCode lastActive;
    public JavaCode (){
        code = "";
    }
    
    public void setCode(String code){
        this.code = code;
    }
    
    public static boolean isUnactive() {
        return lastActive==null;
    }
    
    public String getCode(){
        return code;
    }
    
    public void setActive(){
        if(lastActive!=null){
             lastActive.code = ui.getText();
       }
        else
        {
            ui.setEditable(true);
        }
        lastActive=this;
        ui.setText(code);
    }
    
    public static void setUnactive(){
        if(lastActive!=null){
            lastActive.code = ui.getText();
            lastActive=null;
            ui.setText("");
        }
        ui.setEditable(false);
//        ui.setText("");
    }
    
    public static void Update(){
        if(lastActive!=null)
            lastActive.code=ui.getText();
    }
    
}
