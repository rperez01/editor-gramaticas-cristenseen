/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InternalLogic;

import InternalLogic.Serializers.ImportsSerializer;
import InternalLogic.Serializers.PrecedenceSerializer;
import InternalLogic.Serializers.SemanticHeaderSerializer;
import InternalLogic.Serializers.SymbolNamesSerializer;
import InternalLogic.Serializers.SymbolAttributesSerializer;
import TextChunkDividers.MarkListener;
import TextChunkDividers.TextChunkDivider;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import ide.gc.globalvalues.Imports;
import ide.gc.globalvalues.Precedence;
import ide.gc.globalvalues.SymbolAttributes;
import ide.gc.globalvalues.SymbolNames;
import ide.gc.sintaxLogic.SyntaxMarker;
import ide.gc.ui.GCMenu;
import ide.gc.ui.MainWindow;
import java.io.Reader;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JEditorPane;
import javax.swing.JTextPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;

/**
 * Internal structure of the project
 * @author Ricardo Perez Cortés
 */
public class Project implements MarkListener, DocumentListener{
    
    private transient Gson parser;
    private transient TextChunkDivider linesDivider;
    private transient TextChunkDivider rulesDivider;
    private transient RuleListener rl;
    private transient AbstractDocument doc;
    private transient SyntaxMarker markerInstance;
    private transient LinkedHashSet<Integer> linesUpdate;
    private SymbolNames symbolNames;
    private Precedence precedence;
    private SemanticHeaderSerializer shs;
    private SymbolAttributes symbolAttributes; 
    private ArrayList<SyntaxHeader> headers;
    private Imports imports;
    private transient ImportsSerializer is;
    private transient SymbolNamesSerializer sns;
    private transient SymbolAttributesSerializer sas;
    private transient PrecedenceSerializer ps;
    private transient boolean listenerPermision;
public Project(AbstractDocument doc) {
    
    GCMenu gcm;
    GsonBuilder gson=new GsonBuilder();
    gcm=MainWindow.getWindow().getGCMenu();
    symbolNames=gcm.getSymbolNames();
    symbolAttributes=gcm.getSymbolAttributes();
    imports = gcm.getImports();
    sns=new SymbolNamesSerializer(symbolNames);
    gson.registerTypeAdapter(SymbolNames.class,sns);
    shs=new SemanticHeaderSerializer();
    gson.registerTypeAdapter(SemanticHeaderSerializer.class, shs);
    sas=new SymbolAttributesSerializer(symbolAttributes);
    gson.registerTypeAdapter(SymbolAttributes.class,sas);
    is=new ImportsSerializer(imports);
    gson.registerTypeAdapter(Imports.class,is);
    precedence=gcm.getPrecedence();
    ps=new PrecedenceSerializer(precedence);
    gson.registerTypeAdapter(Precedence.class, ps);
    gson.setLenient();
    gson.disableHtmlEscaping();
    parser=gson.create();
    
    rl=new RuleListener();
    this.doc=doc;
    doc.addDocumentListener(this);
    headers      = new ArrayList<>();
    headers.add(new SyntaxHeader());
    linesUpdate  = new LinkedHashSet();
    listenerPermision=true;
}

public SyntaxHeader[] getHeaders(){
    return headers.toArray(new SyntaxHeader[headers.size()]);
}

/**
 * Serializes the string as JSON
 * @return Strign of the serialization of the project
 */
public String saveAsJSON(){
    String ret;
    JavaCode.setUnactive();
    ret = parser.toJson(this);
    //System.out.println(ret);
    return ret;
}

/**
 * Serializes the string as JSON
 * @return Strign of the serialization of the project
 */
public void loadFromJSON(Reader json){
    listenerPermision=false;
    linesUpdate.clear();
    linesDivider.clear();
    rulesDivider.clear();
    DocumentFilter df=  doc.getDocumentFilter();
    doc.setDocumentFilter(null);
    try {
            
            doc.remove(0, doc.getLength());

    System.out.println(json);
    Project aux = parser.fromJson(json, Project.class);
    this.headers=aux.headers;
    doc.setDocumentFilter(df);
    doc.insertString(0, this.toString(),null);
    } catch (Exception ex) {  
        doc.setDocumentFilter(df);
        Logger.getLogger(Project.class.getName()).log(Level.SEVERE, null, ex);
    }
    listenerPermision=true;
}


public void  setStuff(TextChunkDivider linesDivider, TextChunkDivider rulesDivider, SyntaxMarker syntaxMarker){
    this.linesDivider=linesDivider;
    linesDivider.addListener(this);
    this.rulesDivider=rulesDivider;
    rulesDivider.addListener(rl);
    markerInstance=syntaxMarker;
    
}
    private void updateLine(int position){
        int ruleLine, lineLine, header;
        int startLine, offsetLine;
        String newText;
        int rulePosition ;
        startLine=linesDivider.getPreviousMarkPosition(position);
        offsetLine=linesDivider.getNextMarkPosition(position);
        if(offsetLine==-1)
                offsetLine=doc.getLength();
        offsetLine-=startLine;
        try {
            newText=doc.getText(startLine, offsetLine);
            header=rulesDivider.getNearestMarkIndex(startLine);
            if(markerInstance.isHeader(position-1)){
                

                headers.get(header).setHeader(newText);
            }
            else{
                rulePosition=rulesDivider.getPreviousMarkPosition(position);
                lineLine = linesDivider.getNearestMarkIndex(position-1);
                ruleLine = linesDivider.getNearestMarkIndex(rulePosition);
                
                headers.get(header-1).
                        getLine(lineLine-ruleLine).
                        setLine(newText);
                
            }
        } catch (BadLocationException ex) {
            Logger.getLogger(Project.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public class Container{
        public SyntaxHeader header;
        public SyntaxLine line;
        int relativeLineNumber;
    }
    




    
    
    public Container getLine(int index){
        int counter,lastHeaderNums;
        int headerIndex=0;
        Container ret = new Container();
        counter=lastHeaderNums=0;
        if(index<=0){
            counter=1;
            ret.header=headers.get(0);
        }
        while(counter<=index){
            if(headers.size()==headerIndex)
                return null;
            ret.header=headers.get(headerIndex);
            lastHeaderNums=ret.header.getLineNumber();
            counter+=1+lastHeaderNums;
            headerIndex++;
        }
        headerIndex--;
        counter-=lastHeaderNums;
        ret.relativeLineNumber=index-counter;
        if(index>=counter)
            ret.line=ret.header.getLine(ret.relativeLineNumber);
        else ret.line=null;
        return ret;
    }
    
    @Override
    public void MarkAdded(int index) {
        if(!listenerPermision)
            return;
        int position;
        
        SyntaxLine newLine;
        Container cont;
        
        position=linesDivider.indexToPosition(index)+1;
        if(!markerInstance.isHeader(position)){
            cont= getLine(index-1);
            if(cont.line==null){
                newLine=new SyntaxLine();
            }
            else{
                newLine=cont.line.divide(position-1
                        -linesDivider.getPreviousMarkPosition(position-2));
            }
            //System.out.print(cont.header);
            cont.header.addLine(newLine, cont.relativeLineNumber+1);
            linesUpdate.add(position-1);
            linesUpdate.add(linesDivider.getPreviousMarkPosition(position-2));
        }
        else if(!markerInstance.isHeader(position-1)){
            cont= getLine(index-2);
            if(cont.line==null){
                newLine=new SyntaxLine();
            }
            else{
                newLine=cont.line.divide(position-1
                        -linesDivider.getPreviousMarkPosition(position-2));
            }
            cont.header.addLine(newLine, cont.relativeLineNumber+1);
            linesUpdate.add(position-1);
            linesUpdate.add(linesDivider.getPreviousMarkPosition(position-2));
        }
    }
    
    private void updateLines(){
        if(!listenerPermision)
            return;
        Container aux;
        int init;
        int lenght;
        for(int i: linesUpdate){
            init=linesDivider.getPreviousMarkPosition(i);
            aux=getLine(linesDivider.getNearestMarkIndex(i));
            lenght=linesDivider.getNextMarkPosition(i);
            if(lenght==-1){
                lenght=doc.getLength();
            }
            lenght-=init;
            if(aux.line!=null){
                
                try {
                    aux.line.setLine(doc.getText(init,lenght));
                } catch (BadLocationException ex) {
                    Logger.getLogger(Project.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            else{
                try {
                    aux.header.setHeader(doc.getText(init, lenght));
                } catch (BadLocationException ex) {
                    Logger.getLogger(Project.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        linesUpdate.clear();
    }
    
    @Override
    public void MarkRemoved(int index) {
        if(!listenerPermision)
            return;
        Container cont;
        SyntaxLine sl;
        index--;
        cont = getLine(index);
        if(cont.line!=null ){
            if( cont.header.getLineNumber() == cont.relativeLineNumber+1){
                cont.header.removeLine(cont.relativeLineNumber);
            }
            else{
                sl=cont.header.getLine(cont.relativeLineNumber+1);
                cont.line.merge(sl);
                System.out.print("MERGED LINES YAY");
                cont.header.removeLine(cont.relativeLineNumber+1);
            }
        }
        
    }
    
    @Override
    public void insertUpdate(DocumentEvent e) {
        int inicio,fin;
        if(!listenerPermision)
            return;
        inicio=linesDivider.getPreviousMarkPosition(e.getOffset());
        fin=linesDivider.getPreviousMarkPosition(e.getOffset()+e.getLength());
        linesUpdate.add(inicio);
        linesUpdate.add(fin);
        updateLines();
        
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        if(!listenerPermision)
            return;
        int removalZone=linesDivider.getPreviousMarkPosition(e.getOffset()-1);
        linesUpdate.add(removalZone);
        updateLines();
        
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
    }

    
    

    /**
     * Returns a string that contains all the project syntax
     * @return the project itself
     */
    @Override
    public String toString(){
        String ret="";
        int lineNum;
        for(SyntaxHeader sh :headers){
            ret+=sh.getHeader();
            lineNum=sh.getLineNumber();
                for(int i = 0;i<lineNum;i++){
                    ret+=sh.getLine(i).getLine();
                }
                
        }
        return ret;
    }
    
    
    

    protected class RuleListener implements MarkListener{
        /*ArrayList<SyntaxHeader> headers;
        private TextChunkDivider linesDivider;
        private TextChunkDivider rulesDivider;*/
        @Override
        public void MarkRemoved(int index) {
            if(!listenerPermision)
                return;
            SyntaxHeader beforeHeader = headers.get(index-2);
            SyntaxHeader afterHeader  = headers.get(index-1);
            beforeHeader.merge(afterHeader);
            headers.remove(index-1);
        }

        @Override
        public void MarkAdded(int index) {
            if(!listenerPermision)
                return;
            int positionBefore;
            int position=rulesDivider.indexToPosition(index);
            int pLine;
            int pBLine;
            SyntaxHeader sh;
            if(index!=1){
                pLine=linesDivider.getNearestMarkIndex(position-1);
                positionBefore= rulesDivider.indexToPosition(index-1);
                pBLine= linesDivider.getNearestMarkIndex(positionBefore-1);
                sh=headers.get(index-2).divide(pLine-pBLine-1);
                headers.add(index-1,
                        sh);
                linesUpdate.add(linesDivider.indexToPosition(pLine-1));
                //sh.addLine(new SyntaxLine (), 0);
            }
            else{
                if(rulesDivider.getNextMarkPosition(position)!=-1)
                {
                    headers.add(0, new SyntaxHeader());
                }//.addLine(new SyntaxLine (), 0);
            }
            linesUpdate.add(linesDivider.getPreviousMarkPosition(position));
            linesUpdate.add(position+1);
            //updateLines();
        }
    
    }


}
