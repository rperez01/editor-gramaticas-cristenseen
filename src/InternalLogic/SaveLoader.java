/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InternalLogic;

import ide.gc.sintaxLogic.SyntaxMarker;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * Loads or saves the project to a file
 * @author ricardo
 */
public class SaveLoader implements ActionListener{

    
    JFileChooser jfile;
    public final static String FILE_EXTENSION="cgp";
    static File file=null;
    private final static String EMPTYFILE="{\"symbolNames\":[],\"precedence\":[],\"shs\":\"\",\"symbolAttributes\":[],\"headers\":[{\"header\":\"\",\"lines\":[]}],\"imports\":[]}";
    
    public SaveLoader(){
     jfile=new JFileChooser(".");
                 jfile.setFileSelectionMode(JFileChooser.FILES_ONLY);
                 jfile.setFileFilter(new FileNameExtensionFilter("CHRISTENSEEN GRAMMAL PROJECTS",FILE_EXTENSION));
    }
    @Override
    public void actionPerformed(ActionEvent ae) {
        int event;

        if(ae.getActionCommand()=="Open"){
            jfile.setCurrentDirectory(new java.io.File("."));

            event=jfile.showOpenDialog(null);
            if(event==JFileChooser.APPROVE_OPTION){
                file=jfile.getSelectedFile();
                try {
                    Reader in = new BufferedReader(new InputStreamReader(new FileInputStream(file),Charset.forName("UTF-8")));
                    //char [] fileChars = new char[(int)file.length()];
                    //FileReader fr= new FileReader(file);
                    //in.read(fileChars);

                    SyntaxMarker.getInstance().getProject().loadFromJSON(in);
                    /*SyntaxMarker.getInstance().getProject().loadFromJSON(fr.);
                    fw.write(saveAsJSON());
                    fw.close();*/

                    
                    in.close();
                } catch (IOException ex) {
                    Logger.getLogger(SaveLoader.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        }
        else if(ae.getActionCommand()=="Save"){
            if(file==null){
                event=jfile.showSaveDialog(null);
                if(event==JFileChooser.APPROVE_OPTION){
                    file=jfile.getSelectedFile();
                    if(!file.exists()&&file.getName().indexOf(".")==-1){
                        file=new File (file.getAbsoluteFile()+"."+FILE_EXTENSION);
                    }
                    try {
                        Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file),Charset.forName("UTF-8")));
                        /*FileWriter fw= new FileWriter(file);
                        fw.write(SyntaxMarker.getInstance().getProject().saveAsJSON());
                        fw.close();*/
                        out.write(SyntaxMarker.getInstance().getProject().saveAsJSON());
                        out.close();
                    } catch (IOException ex) {
                        Logger.getLogger(SaveLoader.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            else{
                    try {
                        FileWriter fw= new FileWriter(file);
                        fw.write(SyntaxMarker.getInstance().getProject().saveAsJSON());
                        fw.close();
                    } catch (IOException ex) {
                        Logger.getLogger(SaveLoader.class.getName()).log(Level.SEVERE, null, ex);
                    }
            
            }
        }
        else if(ae.getActionCommand()=="New"){
            file=null;
            StringReader sr= new StringReader(EMPTYFILE);
            SyntaxMarker.getInstance().getProject().loadFromJSON(sr);
            
        }
    }
    
    public static String GetProjectName(){ 
        if(file==null)
            return "Untitled";
        else{
            String[] aux=file.getName().split(".");
            if(aux.length==0)
                return file.getName();
            return aux[0];
        }
    }
    
}
