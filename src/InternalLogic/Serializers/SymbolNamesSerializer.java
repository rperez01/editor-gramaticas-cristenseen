/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InternalLogic.Serializers;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;
import ide.gc.globalvalues.SymbolNames;
import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 *Serializes and deserializes the class symbol names
 * @author Ricardo Pérez Cortés
 */
public class SymbolNamesSerializer implements JsonSerializer<SymbolNames>,JsonDeserializer<SymbolNames>{
    private SymbolNames s;
    private static Gson gs= new Gson();
    public SymbolNamesSerializer(SymbolNames s){
        this.s=s;
        
    }

    @Override
    public JsonElement serialize(SymbolNames src, Type typeOfSrc, JsonSerializationContext context) {
        return gs.toJsonTree(src.saveToArray());


    }

    
    @Override
    public SymbolNames deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        s.loadFromArray(gs.fromJson(json, new TypeToken<ArrayList<SymbolNames.Info>>(){}.getType()));
        return s;
    }
}
