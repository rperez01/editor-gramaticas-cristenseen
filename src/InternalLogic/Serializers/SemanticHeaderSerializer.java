/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InternalLogic.Serializers;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import ide.gc.globalvalues.Precedence;
import ide.gc.ui.MainWindow;
import java.awt.Window;
import java.lang.reflect.Type;

/**
 *
 * @author ricardo
 */
public class SemanticHeaderSerializer implements JsonSerializer<SemanticHeaderSerializer>,JsonDeserializer<SemanticHeaderSerializer>{
    
    private final static Gson gs= new Gson();
        
    @Override
    public JsonElement serialize(SemanticHeaderSerializer t, Type type, JsonSerializationContext jsc) {
        return  gs.toJsonTree(MainWindow.getWindow().getJPAuxiliarDisplay().getSemanticHeader().getText());
        
    }

    @Override
    public SemanticHeaderSerializer deserialize(JsonElement je, Type type, JsonDeserializationContext jdc) throws JsonParseException {
        MainWindow.getWindow().getJPAuxiliarDisplay().getSemanticHeader().setText(je.getAsString());
        return this;
    }
    
}
