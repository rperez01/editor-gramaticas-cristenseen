/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InternalLogic.Serializers;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;
import ide.gc.globalvalues.Precedence;
import ide.gc.globalvalues.SymbolNames;
import ide.gc.ui.GCMenu;
import ide.gc.ui.MainWindow;
import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Serializes and deserializes the Precedence
 * @author Ricardo Pérez Cortés
 */
public class PrecedenceSerializer implements JsonSerializer<Precedence>,JsonDeserializer<Precedence>{
    private Precedence p;
    private final static Gson gs= new Gson();
    public PrecedenceSerializer(Precedence p){
        this.p=p;
        
    }

    @Override
    public JsonElement serialize(Precedence src, Type typeOfSrc, JsonSerializationContext context) {
        return gs.toJsonTree(src.saveToArray());


    }

    
    @Override
    public Precedence deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        p.loadFromArray(gs.fromJson(json, new TypeToken<ArrayList<Precedence.PrecedenceInfo>>(){}.getType()));
        return p;
    }
}
