/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InternalLogic;

import java.util.ArrayList;

/**
 * Internal representation of a header of the syntax
 * @author Ricardo Pérez Cortés
 */
public class SyntaxHeader {
    private String header;
    private ArrayList <SyntaxLine> lines;
    
    public SyntaxHeader(){
        header="";
        lines = new ArrayList();
    }
        
    public void setHeader(String s){
        header = s;
    }
    
    public String getHeader(){
        return header;
    }
    
    public void  addLine(SyntaxLine line , int position){
        lines.add(position,line);
    }
    
    
    /**
     * REturns the number of lines of the header
     * @return the number of lines that the header has 
     */
    public int getLineNumber(){
        return lines.size();
    }
    
    /**
     * Adds the lines of another header to itself
     * @param header 
     */
    public void merge(SyntaxHeader header){
        SyntaxLine sl=new SyntaxLine();
        sl.setLine(header.header);
        lines.add(sl);
        lines.addAll(header.lines);
    }
    /**
     * Divides a header into another one giving splitting itself from a line
     * @param line line where to divide the header
     * @return the new SyntaxHeader
     */
    public SyntaxHeader divide(int line){
        SyntaxHeader ret = new SyntaxHeader();
        /*System.out.print(line);
        System.out.print(lines.size());*/
        if(line+1<lines.size()){
            ret.lines = new  ArrayList(lines.subList(line+1,lines.size()));}
        if(line<lines.size())
            ret.header=this.lines.get(line).getLine();
        lines = new ArrayList(lines.subList(0, line));
        return ret;
    }
    
    
    public SyntaxLine getLine(int lineNumber){
        return lines.get(lineNumber);
    }
    /**
     * Removes a line from the header
     * @param lineNumber the number of line to be removed
     */
    void removeLine(int lineNumber) {
        lines.remove(lineNumber);
    }
}
