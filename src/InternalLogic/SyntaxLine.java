/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InternalLogic;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

/**
 * Internal representation of a header of the syntax
 * @author Ricardo Pérez Cortés
 */
public class SyntaxLine {
    private String line;
    private HashMap<Integer,JavaCode> jc;
    
    public SyntaxLine(){
        line = "";
        jc = new HashMap();
    }
    
    public JavaCode getCode(int i){
        return jc.get(i);
    }
    public void setLine(String s){
        line = s;
    }
    
    public String getLine(){
        return line;
    }
    
    
    /**
     * Returns the maximum number that should have
     * a javacode inserted or saved in this line
     * @return the maximum number
     */
    public int getMaxCodes(){
        char[] aux;
        int quotZone=-1;
        boolean last = false;
        int ret = 1;
        aux=line.trim().toCharArray();
        if(aux.length==0)
            return 0;
        for (char c : aux){
            if(quotZone>=0){
                if(quotZone!='\\')
                quotZone--;
                continue;}
            else if(c=='\''){
                quotZone=1;
            }
            if(c==' '){
                if(!last)
                    ret++;
                last=true;
            }
            else
                last=false;
                
        }
        return ret;
    }
    
    /**
     * Sets some java code attached to them to be active
     * @param number the position on wich this java code should execute
     */
    public void setActiveCode(int number){
        JavaCode java;
        if(number<0)
            return;
        if(jc.containsKey(number)){
            java=jc.get(number);
        }
        else
        {
            java= new JavaCode();
            jc.put(number, java);
        }
        java.setActive();
    }
    /**
     * Merges another line into this one combining the java attributes
     * @param toMerge the line wich is going to merge with this one
     */
    public void merge(SyntaxLine toMerge){
        int otherMaxLength, meMaxLength;
        HashMap<Integer, JavaCode> dict=new HashMap();
        meMaxLength=getMaxCodes();
        otherMaxLength=toMerge.getMaxCodes();
        for (Iterator<Integer> it = jc.keySet().iterator(); it.hasNext();) {
            int key = it.next();
            if(key<meMaxLength){
                dict.put(key, jc.get(key));
            }
        }
        for (Iterator<Integer> it = toMerge.jc.keySet().iterator(); it.hasNext();) {
            int key = it.next();
            if(key<=otherMaxLength){
                dict.put(key+meMaxLength, toMerge.jc.get(key));
            }
        }
        this.jc=dict;
        
    }
        /**
     * Divides one line into two cutting it by a point
     * @param int divideCut the position where is the cut
     */
    public SyntaxLine divide(int divideCut){
        int meMaxLength;
        SyntaxLine sl= new SyntaxLine();
        HashMap<Integer, JavaCode> dict=new HashMap();
        if(divideCut<line.length()){
            sl.line=line.substring(divideCut);
            line=line.substring(0, divideCut);
        }
        else{
            sl.line="";
        }
        meMaxLength=getMaxCodes()+1;
        for (Iterator<Integer> it = jc.keySet().iterator(); it.hasNext();) {
            int key = it.next();
            if(key<meMaxLength){
                dict.put(key, jc.get(key));
            }
            else{
                sl.jc.put(key-meMaxLength, jc.get(key));
            }
        }
        jc=dict;
        return sl;
    }
}
